package LocalValue;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Safe implements Runnable {
    private static ThreadLocal<Date> startDate = new ThreadLocal<Date>() {
        protected Date initialValue() {
            return new Date();
        }
    };
    @Override
    public void run() {
        System.out.printf("\nStarting thread:  %s : %s",Thread.currentThread().getId(),startDate.get());
        try{
            TimeUnit.SECONDS.sleep((int)Math.rint(Math.random())*10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.printf("\nThread finishing: %s : %s",Thread.currentThread().getId(),startDate.get());
    }
}
