package LocalValue;

import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) {
//        UnsafeTask unsafeTask = new UnsafeTask();
//        for (int i=0; i < 5; i++){
//            Thread thread = new Thread(unsafeTask);
//            thread.start();
//            try{
//                TimeUnit.SECONDS.sleep(2);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }

        System.out.println("/******************************************/");
        Safe safeTask = new Safe();

        for (int i=0; i < 5; i++){
            Thread thread = new Thread(safeTask);

            System.out.println(thread.getPriority());
            thread.start();
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
