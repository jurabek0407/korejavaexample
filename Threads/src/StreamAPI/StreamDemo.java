package StreamAPI;

import javafx.print.Collation;

import java.util.ArrayList;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamDemo {
    public static void main(String[] args) {
        ArrayList<Integer> mylist = new ArrayList<>();
        mylist.add(101);
        mylist.add(202);
        mylist.add(330);
        mylist.add(404);
        mylist.add(90);
        mylist.add(3);
        System.out.println(mylist);
        System.out.println("/--------------------------------------------------/");
        Stream<Integer> myStream = mylist.stream();
        Optional<Integer> minVal = myStream.min(Integer::compare);
        if (minVal.isPresent()) {
            System.out.println("Listdagi min value : " + minVal.get());
        }

        Stream<Integer> myStream2 = mylist.stream();
        Optional<Integer> maxValue = myStream2.max(Integer::compare);
        if (maxValue.isPresent()) {
            System.out.println("Listdagi max qiymat : " + maxValue.get());
        }

        System.out.println("/--------------------------------------------------/");

        Stream<Integer> sortedStream = mylist.stream().sorted();
        sortedStream.forEach(a -> System.out.print(a + "\t"));
        System.out.println("\n/--------------------------------------------------/");

        Stream<Integer> oddVals =
                mylist.stream().sorted().filter((n) -> (n % 2) == 1);
        System.out.print("Toq sonlar : ");
        oddVals.forEach(a -> System.out.print(a + "\t"));
        System.out.println("\n/--------------------------------------------------/");
        Stream<Integer> toqSonlar = mylist.stream().sorted().filter(n -> n % 2 == 1).filter(n -> n > 5);
        System.out.print(" 5 dan katta toq sonlar : ");
        toqSonlar.forEach(a-> System.out.print(a));
        System.out.println("\n/--------------------------------------------------/");

    }
}
