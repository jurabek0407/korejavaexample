package StreamAPI;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class ReductionOperation {
    public static void main(String[] args) {
        ArrayList<Integer> myList= new ArrayList<>();
        myList.add(1);
        myList.add(2);
        myList.add(3);
        myList.add(4);
        myList.add(5);
        Optional<Integer> productObj = myList.stream().reduce((a,b)->a*b);
        System.out.println(productObj.get());
        int onj = myList.stream().reduce(1,(a,b)->{
            if (b%2==0) return a*b;
            else return a;
        });
        System.out.println(onj);
        Optional<Integer> obj = Optional.ofNullable(myList.parallelStream().reduce(1, (a, b) -> a * b));
        System.out.println(obj.get());
    }
}
