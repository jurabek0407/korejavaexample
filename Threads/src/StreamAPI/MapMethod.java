package StreamAPI;

import java.util.ArrayList;
import java.util.Spliterator;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class MapMethod {
    public static void main(String[] args) {
        ArrayList<String> myList = new ArrayList<>();
        myList.add("101.1");
        myList.add("142.3");
        myList.add("35.9");
        myList.add("262.8");
        Stream<String> on = myList.parallelStream();
        on.map(s -> 1).forEach(s->System.out.println(s.getClass()));
//        System.out.println(on.count());
//        on.forEach(System.out::println);
    }
}
