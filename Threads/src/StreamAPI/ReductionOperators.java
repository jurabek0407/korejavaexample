package StreamAPI;

import java.util.ArrayList;
import java.util.Optional;

public class ReductionOperators {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(3);
        list.add(2);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        Optional<Integer> productObj = list.stream().reduce((a, b) -> a * b);
        System.out.println("Result by Reduction : " + productObj.get());

        Integer result = list.stream().reduce(1, (a, b) -> a * b);
        System.out.println("Result by reduce function : " + result);

    }
}
