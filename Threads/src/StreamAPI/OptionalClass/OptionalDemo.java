package StreamAPI.OptionalClass;

import java.util.Optional;

public class OptionalDemo {
    public static void main(String[] args) {
        String []words = new String[10];

        Optional<String> checkNull = Optional.of(words[5]);
        if (checkNull.isPresent()){

            String wordLocal=  words[5].toLowerCase();
            System.out.println(wordLocal);
        }
        else{
            System.out.println("word is null");
        }

//        String word = words[5].toLowerCase();
//        System.out.println("Index 5 : "+word);
    }
}
