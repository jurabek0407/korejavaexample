package StreamAPI.PredicateInterface;

import java.util.function.Predicate;

public class Tajriba {
    public static void main(String[] args) {
        Predicate<String> predicate = param->{
            return param.equalsIgnoreCase("Admin");
        };
        System.out.println(predicate.test("ADMIN"));
    }
}
