package StreamAPI.PredicateInterface;

import java.util.function.Predicate;

public class PredicateFunctions {
    public static void main(String[] args) {
        /*
         * ikkita predicat yaratamiz.
         */
        Predicate<Integer> greaterThanTen = (i) -> i > 10;

        Predicate<Integer> lowerThanTwenty = (i) -> i < 20;
        /*
         * test() metodi predikatga mos kelsa true aks holda false qaytaradi.
         */
        System.out.println(" test : " + greaterThanTen.test(1));
        /*
         * obj1.and(Predicat obj2).test(1) obj1 va obj2 true qaytarsa natija ture
         */
        System.out.println(" and : " + greaterThanTen.and(lowerThanTwenty).test(1));
        /*
         * obj1.or(Predicat obj2).test(1) obj1 yoki obj2 true qaytarsa natija ture
         */
        System.out.println(" or : " + greaterThanTen.or(lowerThanTwenty).test(1));

        /*
         * obj1.or(Predicat obj2).test(1) obj1 yoki obj2 true qaytarsa natija ture
         */
        System.out.println(" negative : " + greaterThanTen.negate().test(1));

        /*
         * and va negative va test ni ishlatamiz.
         */
        System.out.println(" and va negative " + greaterThanTen.or(lowerThanTwenty).negate().test(16));


    }
}
