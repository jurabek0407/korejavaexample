package StreamAPI.PredicateInterface.MyExample;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class PredicateStudent {
    public static Predicate<Student> getStudentsByAddress(String address) {
        return p -> p.getAddress().equalsIgnoreCase(address);
    }

    public static Predicate<Student> getStudentBySpeciality(String speciality){
        return p ->p.getSpeciality().equalsIgnoreCase(speciality);
    }

    public static Predicate<Student> getStudentBySmestrBall(int ball){
        return p->p.getSmestrBall() == ball;
    }

    public static List<Student> getStudentsListWithPredicate(List<Student> students, Predicate<Student> predicate){
        return students
                .stream()
                .filter(predicate)
                .collect(Collectors.toList());
    }
}
