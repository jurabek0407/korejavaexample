package StreamAPI.PredicateInterface.MyExample;

public class Student {
    private String fio;
    private String speciality;
    private double scholarship;
    private int smestrBall;
    private String address;

    public Student(String fio, String speciality, double scholarship, int smestrBall, String address) {
        this.fio = fio;
        this.speciality = speciality;
        this.scholarship = scholarship;
        this.smestrBall = smestrBall;
        this.address = address;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public double getScholarship() {
        return scholarship;
    }

    public void setScholarship(double scholarship) {
        this.scholarship = scholarship;
    }

    public int getSmestrBall() {
        return smestrBall;
    }

    public void setSmestrBall(int smestrBall) {
        this.smestrBall = smestrBall;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "\n\tStudent {" +
                "fio='" + fio + '\'' +
                ", scholarship=" + scholarship +
                ", smestrBall=" + smestrBall +
                '}'+"\n";
    }
}
