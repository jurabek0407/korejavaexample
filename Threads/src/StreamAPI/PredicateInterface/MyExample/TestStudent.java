package StreamAPI.PredicateInterface.MyExample;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestStudent {
    public static void main(String[] args) {
        Student student1 = new Student("Muzaffarov Shoxruz Jonibek o'g'li", "SS", 370000, 3, "Sydney");
        Student student2 = new Student("Nizomov Sanjar Mirzohid o'g'li", "AXU", 540000, 4, "London");
        Student student3 = new Student("Kamolov Behruz Begzod o'g'li", "TMI", 740000, 5, "Seul");
        Student student4 = new Student("Kamolova Lola Begzod qizi ", "KKU", 554000, 4, "London");
        Student student5 = new Student("Nizomov Asliddin Mirzohid o'g'li", "AXU", 999000, 4, "New York");
        Student student6 = new Student("Esanov Kamoliddin Husanovich", "AXU", 6567800, 4, "Paris");
        Student student7 = new Student("Zokirov Mirzohid o'g'li", "AXU", 1290000, 4, "London");

        List<Student> list = new ArrayList<>();
        list.addAll(Arrays.asList(new Student[]{student1,student2,student3,student4,student5,student6,student7}));

        System.out.println(PredicateStudent.getStudentsListWithPredicate(list,PredicateStudent.getStudentBySmestrBall(4)));
//        System.out.println(
//                PredicateStudent.getStudentsListWithPredicate(
//                        PredicateStudent.getStudentsListWithPredicate(list,,
//                        PredicateStudent.getStudentBySpeciality("AXU").negate())
//        );

    }
}
