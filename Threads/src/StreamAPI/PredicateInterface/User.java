package StreamAPI.PredicateInterface;

import java.util.function.Predicate;
import java.util.*;

class User {
    String name, role;

    User(String a, String b) {
        name = a;
        role = b;
    }

    String getRole() {
        return role;
    }

    String getName() {
        return name;
    }

    public String toString() {
        return "\n\tUser Name : " + name + ", Role :" + role + "\n";
    }

    public static void main(String args[]) {
        List<User> users = new ArrayList<User>();
        users.add(new User("John", "admin"));
        users.add(new User("Alisa", "admin"));
        users.add(new User("Peter", "member"));
        Predicate<User> predicate = (User u) -> u.getRole().equals("admin");
        System.out.println(predicate.test(users.get(0)));
        List admins = process(users, (User u) -> u.getRole().equals("admin"));
        System.out.println(admins);
    }

    public static List<User> process(List<User> users,
                                     Predicate<User> predicat) {
        List<User> result = new ArrayList<User>();
        for (User user : users)
            if (predicat.test(user))
                result.add(user);
        return result;
    }
}