package FutureAndCallableExample;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Exmaple2 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newSingleThreadExecutor();

        Future<String> future = executorService.submit(() -> {
            Thread.sleep(2000);
            return "Hello from future";
        });

        while (!future.isDone()) {
            System.out.println("Task is still not done!!!");
            Thread.sleep(2000);
        }
        System.out.println(future.cancel(true));

        System.out.println("Task completed ! Retrieveing the result");
        System.out.println(future.get());
        executorService.shutdown();
    }
}
