package FutureAndCallableExample;

import java.util.concurrent.*;

public class FutureAndCallableExample {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Callable<String> callable = ()->{
            System.out.println("Enter the callable!!!");
            Thread.sleep(1000);
            return "Hello from callable";
        };
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        Future<String> future = executorService.submit(callable);
        System.out.println("Some another operation1");
        System.out.println("Some another operation2");
        System.out.println("Some another operation3");
        System.out.println(future.get());
        executorService.shutdown();
    }
}
