package CollableAndFuture.Collable;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class TestCallable {
    public static void main(String[] args) {
        FutureTask[] randomNumberTask = new FutureTask[5];

        for (int i = 0; i < 5; i++) {
            CollableExmaple collableTask = new CollableExmaple();
            randomNumberTask[i] = new FutureTask(collableTask);
            Thread thread = new Thread(randomNumberTask[i]);
            thread.start();
        }

        for (int i=0; i <5; i++){
            try {
                System.out.println(randomNumberTask[i].get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
    }
}
