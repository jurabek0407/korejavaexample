package CollableAndFuture;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Test {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        Future<Integer>f1;
        Future<Double> f2;
        Future<Integer> f3;
        System.out.println("Starting...");
        f1 = executorService.submit(new Sum(10));
        f2 = executorService.submit(new Hupot(5,4));
        f3 = executorService.submit(new Factarial(10));
        try{
            System.out.println(f1.get());
            System.out.println(f2.get());
            System.out.println(f3.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
/*
* доброе утро Aльфия опа
* */