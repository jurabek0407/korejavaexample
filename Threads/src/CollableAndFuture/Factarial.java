package CollableAndFuture;

import java.util.concurrent.Callable;

public class Factarial implements Callable<Integer> {
    int stop;

    public Factarial(int stop) {
        this.stop = stop;
    }

    @Override
    public Integer call() throws Exception {
        int fact = 1;
        for (int i = 2; i <= stop; i++) {
            fact *= i;
        }
        return fact;
    }
}
