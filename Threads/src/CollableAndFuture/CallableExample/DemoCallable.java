package CollableAndFuture.CallableExample;

import java.util.concurrent.Callable;

public class DemoCallable {
    public static void main(String[] args) throws Exception {
        Callable<String> callable = new Callable<String>() {
            @Override
            public String call() throws Exception {
                return "Callable interface!!!";
            }
        };
        System.out.println("String : " + callable.call());

        Callable<Integer> integerCallable = new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return 101;
            }
        };
        System.out.println("Integer : " + integerCallable.call());

        Callable<Double> doubleCallable = () -> {
            Thread.sleep(1000);
            return 101.101;
        };
        System.out.println("Double : " + doubleCallable.call());
    }

}
