package CollableAndFuture;

import java.util.concurrent.Callable;

public class Hupot implements Callable<Double> {

    double side1, side2;

    public Hupot(double side1, double side2) {
        this.side1 = side1;
        this.side2 = side2;
    }

    @Override
    public Double call() throws Exception {
        return side1 * side2;
    }
}
