package ThreadGroup.Example3;

public class DemoThreadGroup extends Thread {
    public DemoThreadGroup(String name, ThreadGroup threadGroup) {
        super(threadGroup,name);
        start();
    }

    @Override
    public void run() {
        for (int i=0; i < 10; i++){
            try {
                sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(Thread.currentThread().getName() + " finished excution");
    }
}
