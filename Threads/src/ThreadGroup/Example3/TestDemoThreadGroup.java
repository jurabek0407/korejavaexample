package ThreadGroup.Example3;

public class TestDemoThreadGroup {
    public static void main(String[] args) {
        ThreadGroup threadGroup = new ThreadGroup("Thread Group");
        DemoThreadGroup demoThreadGroup1 = new DemoThreadGroup("Thread 1",threadGroup);
        DemoThreadGroup demoThreadGroup2 = new DemoThreadGroup("Thread 2",threadGroup);
        DemoThreadGroup demoThreadGroup3 = new DemoThreadGroup("Thread 3",threadGroup);
        DemoThreadGroup demoThreadGroup4 = new DemoThreadGroup("Thread 4",threadGroup);
        System.out.println("Active Thread Count : "+threadGroup.activeCount());
        System.out.println("Acitve Thread MaxPriority : "  + threadGroup.getMaxPriority()) ;
        System.out.println("Acitve Thread is Daemon : "  + threadGroup.isDaemon()) ;

    }
}
