package ThreadGroup.Exmaple1;

public class MyThread extends Thread {
    public MyThread(String name, ThreadGroup threadGroup) {
        super(threadGroup, name);
        start();
    }

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            try{
                sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
