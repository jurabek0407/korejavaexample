package ThreadGroup.Exmaple1;

import java.util.concurrent.ExecutorService;

public class TestMyThread {
    public static void main(String[] args) {
        ThreadGroup threadGroup = new ThreadGroup("parent thread group");
        MyThread myThread1 = new MyThread("one",threadGroup);
        MyThread myThread2 = new MyThread("two",threadGroup);
        MyThread myThread3 = new MyThread("three",threadGroup);
        MyThread myThread4 = new MyThread("four",threadGroup);

        System.out.println("Active thread count : "+threadGroup.activeCount());
        System.out.println("Thread Group name : " + threadGroup.getName());
        System.out.println("Get Max Priority : " + threadGroup.getMaxPriority());



    }
}
