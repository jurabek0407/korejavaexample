package ThreadGroup;

public class ThreadGroupDemo implements Runnable{
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName());
    }

    public static void main(String[] args) {
        ThreadGroup threadGroup = new ThreadGroup("Parent Thread :");
        ThreadGroupDemo threadGroupDemo = new ThreadGroupDemo();

        Thread thread1 = new Thread(threadGroup,threadGroupDemo,"one");
        thread1.start();
        Thread thread2 = new Thread(threadGroup,threadGroupDemo,"two");
        thread2.start();
        Thread thread3 = new Thread(threadGroup,threadGroupDemo,"three");
        thread3.start();
        Thread thread4 = new Thread(threadGroup,threadGroupDemo,"four");
        thread4.start();
        System.out.println("Thread Group name : " + threadGroup.getName());
        System.out.println("*********************************************");
        threadGroup.list();
    }
}
