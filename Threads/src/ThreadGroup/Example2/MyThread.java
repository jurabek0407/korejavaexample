package ThreadGroup.Example2;

public class MyThread extends Thread {
    public MyThread(String name, ThreadGroup threadGroup) {
        super(threadGroup,name);
        start();
    }

    @Override
    public void run() {
        for (int i=0; i < 10; i++){
            try{
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
