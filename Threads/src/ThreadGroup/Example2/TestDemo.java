package ThreadGroup.Example2;

import java.util.concurrent.Executors;

public class TestDemo {
    public static void main(String[] args) throws InterruptedException {
        ThreadGroup threadGroup = new ThreadGroup("Parent Group");
//        ThreadGroup child_group = new ThreadGroup(threadGroup,"Child Group");

        MyThread myThread1 = new MyThread("Thread 1",threadGroup);
        MyThread myThread2 = new MyThread("Thread 2",threadGroup);
        MyThread myThread3 = new MyThread("Thread 3",threadGroup);
        System.out.println(threadGroup.activeCount());
        threadGroup.list();

    }
}
