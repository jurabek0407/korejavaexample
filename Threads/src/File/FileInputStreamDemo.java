package File;

import java.io.*;
import java.nio.Buffer;
import java.nio.channels.Channels;

public class FileInputStreamDemo {
    public static void main(String[] args) {
        int size;

        try (FileOutputStream f = new FileOutputStream("input.txt")) {
            String satr = "salom";
            byte[] array = satr.getBytes();
            f.write(array);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
