package File;

import java.io.File;

public class ExampleDirectory {
    public static void main(String[] args) {
        String dirName = "java";
        File file = new File(dirName);
        if (file.isDirectory()) {
            String[] otherFilesOrDirectory = file.list();
            for (int i = 0; i < otherFilesOrDirectory.length; i++) {
                File fileItem = new File(dirName + "/" + otherFilesOrDirectory[i]);
                if (fileItem.isDirectory()) {
                    System.out.println(otherFilesOrDirectory[i] + " Directory!!!");
                }
                else {
                    System.out.println(otherFilesOrDirectory[i] + " File!!!");
                }
            }
        } else {
            System.out.println("Directory topilmadi!!!!");
        }
    }
}
