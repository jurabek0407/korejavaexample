package File;

import java.io.Closeable;
import java.io.File;
import java.io.FilenameFilter;
import java.io.Flushable;

public class FilenameFilterExample {
    public static void main(String[] args) {
        String dirName = "java";
        File file = new File(dirName);
        FilenameFilter only = new OnlyExt("txt");
        String[] files = file.list(only);
        for (String item :
                files) {
            System.out.println(item);
        }
    }
}

class OnlyExt implements FilenameFilter {
    String ext;

    public OnlyExt(String ext) {
        this.ext = "." + ext;
    }

    @Override
    public boolean accept(File dir, String name) {
        return name.endsWith(ext);
    }
}
