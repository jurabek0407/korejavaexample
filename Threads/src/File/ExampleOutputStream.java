package File;

import com.sun.jndi.toolkit.url.Uri;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ExampleOutputStream {
    public static void main(String[] args) throws IOException {
        File file = new File("input.txt");
        System.out.println("File name : " +file.getName());
        System.out.println("Path : " + file.getPath());
        System.out.println("Absolute Path : " + file.getAbsolutePath());
        System.out.println("Parent : " + file.getParent());
        System.out.println("Exists or does not exists: " + file.exists());
        System.out.println("Can write or not :" +file.canWrite() );
        System.out.println("Can read or not :" +file.canRead() );
        System.out.println("Directory or not :" +file.isDirectory());
        System.out.println("File or not :" +file.isFile());
        System.out.println("Last Modified :" +file.lastModified());
        System.out.println("File size :" +file.length());

        System.out.println("Total space : " + file.getTotalSpace());
        System.out.println("Get free space : " + file.getFreeSpace());
        System.out.println("Get usable Space : " +file.getUsableSpace());
        System.out.println(" Is hidden : " + file.isHidden());
        System.out.println(" Is hidden : " + file.setReadOnly());
        System.out.println(" Is hidden : " + file.setWritable(true));
//        System.out.println("Set read Only : " +file.setReadable(true) );

        System.out.println("To Path : "  + file.toPath());
        try (FileOutputStream f = new FileOutputStream("input.txt")) {
            String satr = "salom";
            byte[] array = satr.getBytes();
            f.write(array);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        /*
            File file= new File("text.txt");
            File file1 = new File("/Java","text.txt");
            File file2 = new File("/");
            File file3 = new File(file2,"text.txt");
        */
    }
}
