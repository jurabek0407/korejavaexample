package Future;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class ThreadPool {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Consumer<List<Integer>> modify = list -> {
            for (int i = 0; i < list.size(); i++) {
                list.set(i, 2 * list.get(i));
            }
        };

        Consumer<List<Integer>> print = list -> {
            for (int i = 0; i < list.size(); i++) {
                System.out.println(list.get(i));
            }
        };

        List<Integer> list = new ArrayList<>();
        list.add(101);
        list.add(202);
        list.add(303);
        list.add(404);
        modify.andThen(print).accept(list);

    }

    public static CompletableFuture<String> calculateAsynch() {
        CompletableFuture<String> completableFuture = new CompletableFuture<>();
        Executors.newCachedThreadPool().submit(() -> {

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            completableFuture.complete("Hello");

        });
        return completableFuture;
    }
}

