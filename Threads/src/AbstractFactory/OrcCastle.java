package AbstractFactory;

public class OrcCastle implements Castle {
    static final String DESCRIPTION = "This is orc Castle";
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }
}
