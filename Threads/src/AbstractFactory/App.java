package AbstractFactory;

public class App {

    private King king;
    private Castle castle;
    private Army army;

    private void createKingdom(final KingdomFactory kingdomFactory) {
        setArmy(kingdomFactory.createArmy());
        setCastle(kingdomFactory.createCastle());
        setKing(kingdomFactory.createKing());
    }

    private void setKing(final King king) {
        this.king = king;
    }

    private King getKing() {
        return this.king;
    }

    private void setArmy(final Army army) {
        this.army = army;
    }

    private Army getArmy() {
        return this.army;
    }

    private void setCastle(final Castle castle) {
        this.castle = castle;
    }

    private Castle getCastle() {
        return this.castle;
    }

    public static class FactoryMaker {
        public enum KingdomType {
            ELF, ORC;
        }

        public static KingdomFactory makeFactory(KingdomType type) {
            switch (type) {
                case ELF:
                    return new ElfKindomFactory();
                case ORC:
                    return new OrcKingdomFactory();
                default:
                    throw new IllegalArgumentException("KingdomType not supported.");
            }
        }
    }

    public static void main(String[] args) {

        App app = new App();

        System.out.println("Elf Kingdom");
        app.createKingdom(FactoryMaker.makeFactory(FactoryMaker.KingdomType.ELF));
        System.out.println(app.getArmy().getDescription());
        System.out.println(app.getCastle().getDescription());
        System.out.println(app.getKing().getDescription());

        System.out.println("Orc Kingdom");
        app.createKingdom(FactoryMaker.makeFactory(FactoryMaker.KingdomType.ORC));
        System.out.println(app.getArmy().getDescription());
        System.out.println(app.getCastle().getDescription());
        System.out.println(app.getKing().getDescription());
    }

}
