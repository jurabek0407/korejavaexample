package AbstractFactory;

public class ElfKing implements King{
    static final String DESCRIPTION = "This is the Elven King";
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }
}
