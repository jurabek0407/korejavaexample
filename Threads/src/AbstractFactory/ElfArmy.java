package AbstractFactory;

public class ElfArmy implements Army {
    static final String DESCRIPTION = "This is Elven Army!";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }
}
