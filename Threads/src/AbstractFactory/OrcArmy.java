package AbstractFactory;

public class OrcArmy implements Army {
    static final String DESCRIPTION = "This is Orc Army";
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }
}
