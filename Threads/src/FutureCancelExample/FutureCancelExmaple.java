package FutureCancelExample;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class FutureCancelExmaple {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ExecutorService service = Executors.newSingleThreadExecutor();
        long time = System.nanoTime();

        Future<String> future = service.submit(() -> {
            System.out.println("Entered Callable method");
            return "Hello from Callable";
        });

        while (!future.isDone()) {
            System.out.println("Task is still Running...");
            Thread.sleep(200);
            double differentTime = (System.nanoTime() - time) / 1_000_000_000.0;
            if (differentTime > 1) {
                future.cancel(true);
            }
        }

        System.out.println(" Task completed! Retrieving the result!!!");
        String result = future.get();
        System.out.println(result);

        service.shutdown();
    }
}
