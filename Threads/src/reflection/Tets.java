package reflection;

public class Tets {
    private String s;

    public Tets() {
        this.s = "Geeks for geeks";
    }
    public Tets(int age) {
        this.s = "Geeks for geeks";
    }
    public void method1(){
        System.out.println("The string is " + s);
    }
    public void method2(int n)  {
        System.out.println("The number is " + n);
    }

    private void method3() {
        System.out.println("Private method invoked");
    }
}
