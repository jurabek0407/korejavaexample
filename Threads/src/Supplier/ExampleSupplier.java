package Supplier;

import java.util.Random;
import java.util.function.DoubleSupplier;
import java.util.function.Supplier;

public class ExampleSupplier {
    public static void main(String[] args) {
        Supplier<String> supplier = ()->{
            return "Rupplier";
        };
        System.out.println(supplier.get());
        DoubleSupplier supplier1 = ()->{
            return 101.101;
        };
        System.out.println(supplier1.getAsDouble());

    }
}
