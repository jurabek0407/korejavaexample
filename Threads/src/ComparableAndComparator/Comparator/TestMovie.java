package ComparableAndComparator.Comparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TestMovie {
    public static void main(String[] args) {
        List<Movie> list = new ArrayList<>();
        list.add(new Movie(1,"Qilich va Qalqon",1975));
        list.add(new Movie(10,"Kingsman",2000));
        list.add(new Movie(19,"Jasur",2005));
        list.add(new Movie(13,"Baron",2016));
        list.add(new Movie(8,"Chegara",2017));
        list.add(new Movie(12,"Qasamyod",2015));

        System.out.println("----------------Start State---------------");
        list.stream().forEach(a-> System.out.println(a));
        Collections.sort(list,new RatingCompare());
        System.out.println("----------------End State ( order by rating)---------------");
        list.stream().forEach(a-> System.out.println(a));
    }
}

class RatingCompare implements Comparator<Movie> {

    @Override
    public int compare(Movie o1, Movie o2) {
        if (o1.getRating() - o2.getRating() < 0)
            return -1;
        if (o1.getRating() - o2.getRating() > 0)
            return 1;
        else
            return 0;
    }
}

class NameCompare implements Comparator<Movie> {

    @Override
    public int compare(Movie o1, Movie o2) {
        return o1.getName().compareTo(o2.getName());
    }
}
