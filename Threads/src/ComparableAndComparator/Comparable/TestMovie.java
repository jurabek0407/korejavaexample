package ComparableAndComparator.Comparable;

import java.util.ArrayList;
import java.util.Collections;

public class TestMovie {
    public static void main(String[] args) {
        ArrayList<Movie> list = new ArrayList<>();
        list.add(new Movie(1,"Otamdan qolgan dalalar",2001));
        list.add(new Movie(2,"Qilich va Qalqon",2003));
        list.add(new Movie(3,"Kingsman",2000));
        list.add(new Movie(4,"Jasur",2012));
        list.add(new Movie(5,"Baron",2009));
        System.out.println("------------Start State-------------");
        list.stream().forEach(a-> System.out.println(a));
        Collections.sort(list);
        System.out.println("------------End State-------------");
        list.stream().forEach(a-> System.out.println(a));
    }
}
