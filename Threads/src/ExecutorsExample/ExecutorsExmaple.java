package ExecutorsExample;

import java.sql.Time;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ExecutorsExmaple {
    public static void main(String[] args) {
        System.out.println("Inside " + Thread.currentThread().getName());

        ExecutorService executorService = Executors.newFixedThreadPool(2);

        Runnable ran1 =()->{
            System.out.println("Executing  Task1 inside  : " + Thread.currentThread().getName());
            try{
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };

        Runnable ran2 = ()->{
            System.out.println("Executing Task2 inside : " + Thread.currentThread().getName());
            try{
                TimeUnit.SECONDS.sleep(4);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };

        Runnable ran3 = ()->{
            System.out.println("Executing Task3 inside : " +Thread.currentThread().getName());
            try{
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
        executorService.submit(ran1);
        executorService.submit(ran2);
        executorService.submit(ran3);

        executorService.shutdown();
    }
}
