package ExecutorsExample;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SingleThreadExecutor {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Runnable runnable = () -> {
            System.out.println("Hello from Runnable ");
        };

        executorService.submit(runnable);
    }
}
