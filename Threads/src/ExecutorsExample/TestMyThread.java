package ExecutorsExample;

import java.util.concurrent.CountDownLatch;

public class TestMyThread {
    public static void main(String[] args) {
        CountDownLatch latch = new CountDownLatch(5);
        System.out.println("Starting !!!");
        new MyThread(latch);
        try{
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Done");
    }
}
