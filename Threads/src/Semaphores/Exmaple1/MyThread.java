package Semaphores.Exmaple1;

import Semaphores.Exmaple1.SHared;

import java.util.concurrent.Semaphore;

public class MyThread extends Thread {
    Semaphore sem;
    String threadName;

    public MyThread(Semaphore sem, String threadName) {
        super(threadName);
        this.threadName = threadName;
        this.sem = sem;

    }

    @Override
    public void run() {
        if (this.getName().equals("A")) {
            System.out.println("Starting " + threadName);
            try {
                System.out.println(threadName + " is waiting for permit.");
                sem.acquire();
                System.out.println(threadName + " is permit");
                for (int i = 0; i < 5; i++) {
                    SHared.count++;
                    System.out.println(threadName + " : " + SHared.count);
                    Thread.sleep(10);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(threadName + " release the permit ");
            sem.release();
        } else {
            System.out.println(threadName + " Starting");
            try {
                System.out.println(threadName + " is waiting for permit");
                sem.acquire();
                System.out.println(threadName + " get permit");
                for (int i = 0; i < 5; i++) {
                    SHared.count--;
                    System.out.println(threadName + " : " + SHared.count);
                    Thread.sleep(10);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(threadName + " release the permit");
            sem.release();
        }
    }

}
