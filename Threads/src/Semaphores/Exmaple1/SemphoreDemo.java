package Semaphores.Exmaple1;

import java.util.concurrent.Semaphore;

public class SemphoreDemo {
    public static void main(String[] args) throws InterruptedException {
        Semaphore semaphore = new Semaphore(1);
        MyThread myThread1 = new MyThread(semaphore,"A");
        MyThread myThread2 = new MyThread(semaphore,"B");
        myThread1.start();
        myThread2.start();

        ThreadGroup threadGroup = new ThreadGroup("ParentThread Group");

        myThread1.join();
        myThread2.join();

        System.out.println("count: " + SHared.count);
    }
}
