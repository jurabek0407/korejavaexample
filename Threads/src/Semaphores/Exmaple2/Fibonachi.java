package Semaphores.Exmaple2;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

public class Fibonachi {
    public static void main(String[] args) {
        ForkJoinPool forkJoinPool = new ForkJoinPool(Runtime.getRuntime().availableProcessors());
        System.out.println(forkJoinPool.invoke(new Sum(10)));

    }
}
class Sum extends RecursiveTask<Integer> {
    int n;

    public Sum(int n) {
        this.n = n;
    }

    @Override
    protected Integer compute() {
        if (n<=2)
            return 1;
        Sum sum = new Sum(n-1);
        sum.fork();
        Sum sum1 = new Sum(n-2);
        sum1.fork();
        return sum.join() + sum1.join();
    }
}
