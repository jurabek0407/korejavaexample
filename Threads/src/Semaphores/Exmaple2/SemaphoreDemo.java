package Semaphores.Exmaple2;

import java.util.concurrent.Semaphore;

public class SemaphoreDemo {
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(3,true);
        Thread thread[] = new MyThread[12];
        for (int i = 0; i < 12; i++) {
            thread[i] = new MyThread(semaphore);
        }
        for (int i = 0; i < 12; i++) {
            thread[i].start();
        }

    }
}
