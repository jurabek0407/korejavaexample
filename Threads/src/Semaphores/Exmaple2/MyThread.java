package Semaphores.Exmaple2;

import java.util.concurrent.Semaphore;

public class MyThread extends Thread {
    Semaphore semaphore;

    public MyThread(Semaphore semaphore) {
        this.semaphore = semaphore;
    }

    @Override
    public void run() {
        try {
            semaphore.acquire();
            System.out.println("Hello : " + this.getName());
            try {
                sleep(5000);
            } catch (InterruptedException es) {
                System.out.println(es);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            System.out.println("Goodbye " + this.getName());
            semaphore.release();
        }
    }
}
