package Semaphores;

import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

public class VektorSum {

    public static void main(String[] args) {

        int n = 99999996;
        int[] array = new int[n];
        long time = System.nanoTime();
        long correctSum = 0;
        for (int i = 0; i < n; i++) {
            array[i] = i;
            correctSum += i;
        }
        System.out.println(System.nanoTime() - time);
        long finalCorrectSum = correctSum;
        MyThread[] threads = new MyThread[4];
        Thread print = new Thread(() -> {
            long tim = System.nanoTime();
            for (int i = 0, k = 0; i < 4; i++, k = k + n / 4) {
                threads[i] = new MyThread(k, (i + 1) * n / 4, array);
                threads[i].start();
            }
//            for (MyThread thread : threads) {
                try {
                    threads[0].join();
                    threads[1].join();
                    threads[2].join();
                    threads[3].join();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
//            }
            long sum = 0;
            System.out.println(System.nanoTime() - tim);
            for (MyThread thread : threads) {
                System.out.println(thread.getName() + " = " + thread.getSum());
                sum += (thread.getSum());
            }
            System.out.println(System.nanoTime() - tim);
            System.out.println();
            System.out.println("\ncorrectSum = " + finalCorrectSum);
            System.out.println("\nsum = " + sum);
        });
        print.start();
//        while (true) {
//            if (threads[3].getState() == Thread.State.TERMINATED) {
//                System.out.println("\n Result : " + MyThread.sum);
//                System.out.println("\n Old sum : " + correctSum);
//                break;
//            }
//        }
    }
}

class MyThread extends Thread {

    int startIndex;
    int endIndex;
    int array[];
    private long sum = 0;


    public MyThread(int startIndex, int endIndex, int[] array) {
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.array = array;
    }

    @Override
    public void run() {
        for (int i = startIndex; i < endIndex; i++) {
            System.out.println(Thread.currentThread().getName() + " " + array[i]);
            sum += (array[i]);
        }
        System.out.println();
    }

    public Long getSum() {
        return sum;
    }
}
