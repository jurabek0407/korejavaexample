package CompletableFutures;

import java.util.concurrent.*;

public class MultipleThenApply {
    public static void main(String[] args) {
        CompletableFuture<String> start = CompletableFuture.supplyAsync(()->{
            try {
                TimeUnit.SECONDS.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "Start ...";
        }).thenApply(name->{
            return name +  "CompletableFuture class thenApply method";
        }).thenApply(getting->{
            return getting + " adn give up";
        });
    }
}
