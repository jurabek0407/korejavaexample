package CompletableFutures;

import java.util.ArrayList;
import java.util.concurrent.*;

public class JumaKuniGapiraman {
    public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException {

        /*
         *  nima uchun har safar ishlatganda har xil javoblar chiqyapti
         */
        ExecutorService executorService = Executors.newFixedThreadPool(4);

        CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
            System.out.println("This is run method");
        }, executorService);
        executorService.shutdown();
        /*
         * 2-misol supplyAsunc() meotodi bu qiymat olmaydi lekin qaytaradi.
         */
        CompletableFuture<Void> supplyAsync = CompletableFuture.supplyAsync(() -> {
            ArrayList<Integer> arrayList = new ArrayList<>();
            arrayList.add(101);
            arrayList.add(202);
            arrayList.add(303);
            System.out.println("Birinchi");
            return arrayList;
        }).thenApply(name -> {
            System.out.println("ikkinchi");
            name.add(909);
            return name;
        }).thenApply(name2 -> {
            name2.add(10101);
            System.out.println("Uchinchi");
            return name2;
        }).thenAccept(list->{
            System.out.println(list.size());
        });
        /*
         *  3-misol: supplyAsunc bilan thenAcceptni ishlatamiz.
         *  Void bulganini sababi thenAccept void tipini qullab quvvatlaydi.
         */

        CompletableFuture<Void> obj2 = CompletableFuture.supplyAsync(() -> {
            System.out.println("Hello from");
            ArrayList<Integer> arrayList = new ArrayList<Integer>();
            arrayList.add(101);
            arrayList.add(202);
            arrayList.add(404);
            return arrayList;
        }).thenAccept((name) -> {
            name.stream().forEach(a -> System.out.println(a));
        });
    }
}
