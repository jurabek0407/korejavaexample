package CompletableFutures;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class thenApply {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        CompletableFuture<String> completableFuture = CompletableFuture.supplyAsync(() -> {
            return "thenApply completableFuture";
        });
//        ExecutorService ec= Executors.newFixedThreadPool(100000000);
        CompletableFuture<String> completableFuture1 = completableFuture.thenApplyAsync(name -> {
            return "Start -->> " + name;
        });
        System.out.println(completableFuture1.get());
//        ec.shutdown();
    }
}
