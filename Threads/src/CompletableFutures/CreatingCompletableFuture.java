package CompletableFutures;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class CreatingCompletableFuture {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        /*
         *  biz runAsync method orqali main thread ddan alohida ishlash imkonini beradi
         *  Void qiymat qaytarmaydi Runnable object oladi.
         */

        CompletableFuture<Void> future = CompletableFuture.runAsync(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("This is runAsync method()");
            }
        });
        CompletableFuture<Void> future1 = CompletableFuture.runAsync(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("This is runAsync method wrote lambda ");
        });


    }
}
