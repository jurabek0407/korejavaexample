package CompletableFutures;

import java.sql.Time;
import java.util.concurrent.*;
import java.util.function.Supplier;

public class SypplyCompletableFuture {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        CompletableFuture<String> future = CompletableFuture.supplyAsync(new Supplier<String>() {
            @Override
            public String get() {
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return "Result of asynchronous computation";
            }
        });

        System.out.println(future.get());
        ExecutorService executor = Executors.newFixedThreadPool(10);
        CompletableFuture<String> future1 = CompletableFuture.supplyAsync(()->{
            return "asdasd";

        },executor);

        System.out.println(future1.get());
        executor.shutdown();

    }
}
