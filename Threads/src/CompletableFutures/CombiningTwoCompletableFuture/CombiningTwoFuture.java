package CompletableFutures.CombiningTwoCompletableFuture;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class CombiningTwoFuture {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        CompletableFuture<Double> weight = CompletableFuture.supplyAsync(() -> {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return 20.0;
        });

        CompletableFuture<Double> height = CompletableFuture.supplyAsync(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return 80.0;
        });

        CompletableFuture<Double> combinedFuture = weight
                .thenCombine(height, (param1, param2) -> {
                    System.out.println("param 1 : " + param1);
                    System.out.println("param 2 : " + param2);
                    return param1 * param2;
                });

        System.out.println(combinedFuture.get());
    }
}
