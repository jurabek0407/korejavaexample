package CompletableFutures.CombiningTwoCompletableFuture;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class CombineTwoFuture {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        CompletableFuture<String> obj1 = CompletableFuture.supplyAsync(() -> {
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "Future 1";
        });

        CompletableFuture<String> obje2 = CompletableFuture.supplyAsync(() -> {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "Future 2";
        });

        CompletableFuture<String> obj3 = CompletableFuture.supplyAsync(() -> {
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "Future 3";
        });

        CompletableFuture<Object> result = CompletableFuture.anyOf(obj3,obje2,obj1);

        System.out.println(result.get());
    }
}
