package Pattern.compositePattern;

import java.util.ArrayList;
import java.util.List;

public class HeadDepartment implements Department {
    String name;
    Integer id;

    private List<ChildDepartment> headDepartment;

    public HeadDepartment(Integer id, String name) {
        this.name = name;
        this.id = id;
        this.headDepartment = new ArrayList<>();
    }

    @Override
    public void printDepartmentName() {
        for (ChildDepartment item : headDepartment
        ) {
            item.print();
        }
    }

    public void addDepartment(ChildDepartment department) {
        headDepartment.add(department);
    }

    public void removeDepartment(List<Department> department) {
        headDepartment.remove(department);
    }
}
