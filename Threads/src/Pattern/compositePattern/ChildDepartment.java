package Pattern.compositePattern;

import java.util.ArrayList;
import java.util.List;

public class ChildDepartment {

    private String name;

    private Integer id;

    private List<Department> childDepartment;

    public ChildDepartment( Integer id,String name) {
        this.name = name;
        this.id = id;
        childDepartment =new ArrayList<>();
    }

    public void addDepartment(Department department){
        childDepartment.add(department);
    }

    public void removeDeapartment(Department department ){
        childDepartment.remove(department);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void print(){
        boolean h=true;
        for (Department item :
                childDepartment) {
            if (h){
                System.out.println(item.getClass().getSimpleName());
                h=false;
            }
            item.printDepartmentName();
        }
    }
}
