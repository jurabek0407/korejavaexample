package Pattern.compositePattern;

import java.util.List;

public class    Run {
    public static void main(String[] args) {

        HeadDepartment headDepartment = new HeadDepartment(303, "Head Department");

        ChildDepartment financialDepartment = new ChildDepartment(101, "Financial Department");
        financialDepartment.addDepartment(new FinancialDepartment(1011, "Financial Department 1"));
        financialDepartment.addDepartment(new FinancialDepartment(1012, "Financial Department 2"));
        financialDepartment.addDepartment(new FinancialDepartment(1013, "Financial Department 3"));

        ChildDepartment salesDepartment = new ChildDepartment(202, " Sales Department");
        salesDepartment.addDepartment(new SalesDepartment(1011, "Sales Department 1"));
        salesDepartment.addDepartment(new SalesDepartment(1012, "Sales Department 2"));
        salesDepartment.addDepartment(new SalesDepartment(1013, "Sales Department 3"));

        headDepartment.addDepartment(financialDepartment);
        headDepartment.addDepartment(salesDepartment);
        headDepartment.printDepartmentName();

    }
}
