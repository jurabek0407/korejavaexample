package Pattern.compositePattern;

public interface Department {
    void printDepartmentName();
}
