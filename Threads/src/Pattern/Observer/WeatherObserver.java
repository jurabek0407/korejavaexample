package Pattern.Observer;
public interface WeatherObserver {

    void update(WeatherType currentWeather);

}