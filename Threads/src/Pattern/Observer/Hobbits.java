package Pattern.Observer;

public class Hobbits implements WeatherObserver {
    @Override
    public void update(WeatherType currentWeather) {
        switch (currentWeather) {
            case COLD:
                System.out.println("The Hobbits are freezing cold.");
                break;
            case RAINY:
                System.out.println("The Hobbits are dripping wet.");
                break;
            case SUNNY:
                System.out.println("The Hobbits hurts the orcs' eyes.");
                break;
            case WINDY:
                System.out.println("The Hobbits smell almost vanishes in the wind.");
                break;
            default:
                break;
        }
    }
}