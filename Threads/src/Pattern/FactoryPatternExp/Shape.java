package Pattern.FactoryPatternExp;

public interface Shape {
    void draw();
}
