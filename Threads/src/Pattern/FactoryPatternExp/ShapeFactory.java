package Pattern.FactoryPatternExp;

public class ShapeFactory {
    public Shape getShape(String shapeType){
        if (shapeType == null){
            return null;
        }
        if (shapeType == "Circle"){
            return new Circle();
        }
        if (shapeType == "Rectangle"){
            return new Rectangle();
        }
        return null;
    }
}
