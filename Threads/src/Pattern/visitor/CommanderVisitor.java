package Pattern.visitor;
public class CommanderVisitor implements UnitVisitor {


    @Override
    public void visitSoldier(Soldier soldier) {
        // Do nothing
    }

    @Override
    public void visitSergeant(Sergeant sergeant) {
        // Do nothing
    }

    @Override
    public void visitCommander(Commander commander) {
        System.out.printf("\nGood to see you %s",commander );
    }
}