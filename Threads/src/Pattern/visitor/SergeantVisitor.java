package Pattern.visitor;
public class SergeantVisitor implements UnitVisitor {


    @Override
    public void visitSoldier(Soldier soldier) {
        // Do nothing
    }

    @Override
    public void visitSergeant(Sergeant sergeant) {
        System.out.printf("\nHello %s", sergeant);
    }

    @Override
    public void visitCommander(Commander commander) {
        // Do nothing
    }
}