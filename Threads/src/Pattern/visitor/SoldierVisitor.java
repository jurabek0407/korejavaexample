package Pattern.visitor;
public class SoldierVisitor implements UnitVisitor {


    @Override
    public void visitSoldier(Soldier soldier) {
        System.out.printf("\nGreetings %s", soldier);
    }

    @Override
    public void visitSergeant(Sergeant sergeant) {
        // Do nothing
    }

    @Override
    public void visitCommander(Commander commander) {
        // Do nothing
    }
}