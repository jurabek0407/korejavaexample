package Pattern.ChainOfResponsibility;

public enum RequestType {
    DEFEND_CASTLE,          // qasrni himoya qilish.
    TORTURE_PRISONER,       // qiynoqqa mahkum.
    COLLECT_TAX             // soliq yig'moq.
}
