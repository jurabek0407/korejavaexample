package Pattern.ChainOfResponsibility.example;

public enum RequestType {
    DEFEND_CASTLE,
    TORTUNE_PRISONER,
    COLLECT_TAX
}
