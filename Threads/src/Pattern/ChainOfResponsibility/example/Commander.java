package Pattern.ChainOfResponsibility.example;

public class Commander extends RequestHandled {
    public Commander(RequestHandled next) {
        super(next);
    }

    @Override
    public void handleRequest(Request request) {
        if (request.getRequestType().equals(RequestType.DEFEND_CASTLE)){
            printHandling(request);
            request.setHandled(true);
        }else{
            super.handleRequest(request);
        }
    }

    @Override
    public String toString() {
        return "Commander";
    }
}
