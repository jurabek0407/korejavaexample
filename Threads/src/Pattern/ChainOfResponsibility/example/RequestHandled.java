package Pattern.ChainOfResponsibility.example;

public abstract class RequestHandled {
    RequestHandled next;

    public RequestHandled(RequestHandled next) {
        this.next = next;
    }

    public void handleRequest(Request request) {
        if (next != null) {
            next.handleRequest(request);
        }
    }

    protected void printHandling(Request request) {
        System.out.println(" handling request " + this + "  Request " + request.getDescription());
    }
    public abstract  String toString();
}

