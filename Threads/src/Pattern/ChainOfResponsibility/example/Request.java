package Pattern.ChainOfResponsibility.example;

public class Request {
    private RequestType requestType;
    private String description;
    private boolean handled;

    public Request(RequestType requestType, String description) {
        this.requestType = requestType;
        this.description = description;
    }

    public RequestType getRequestType() {
        return requestType;
    }

    public String getDescription() {
        return description;
    }

    public boolean isHandled() {
        return handled;
    }

    public void setHandled(boolean handled) {
        this.handled = handled;
    }
}
