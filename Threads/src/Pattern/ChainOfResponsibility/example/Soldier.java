package Pattern.ChainOfResponsibility.example;

public class Soldier extends RequestHandled {
    public Soldier(RequestHandled next) {
        super(next);
    }

    @Override
    public void handleRequest(Request request) {
        if (request.getRequestType().equals(RequestType.COLLECT_TAX)) {
            printHandling(request);
            request.setHandled(true);
        } else {
            super.handleRequest(request);
        }
    }

    public String toString() {
        return "Soldier";
    }
}
