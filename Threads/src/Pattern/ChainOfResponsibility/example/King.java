package Pattern.ChainOfResponsibility.example;

public class King {
    private RequestHandled chain;

    public King() {
        buildChain();
    }
    private void buildChain(){
        chain = new Commander(new Officer(new Soldier(null)));
    }
    public void makeRequest(Request request){
        chain.handleRequest(request);
    }
}
