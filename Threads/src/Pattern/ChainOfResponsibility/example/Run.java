package Pattern.ChainOfResponsibility.example;

public class Run {
    public static void main(String[] args) {
        King king = new King();
        king.makeRequest(new Request(RequestType.COLLECT_TAX,"Collect tax"));
        king.makeRequest(new Request(RequestType.DEFEND_CASTLE,"Qasrni himoya qil"));
    }
}
