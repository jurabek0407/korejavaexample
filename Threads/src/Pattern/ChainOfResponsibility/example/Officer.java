package Pattern.ChainOfResponsibility.example;

public class Officer extends RequestHandled {

    public Officer(RequestHandled next) {
        super(next);
    }

    @Override
    public void handleRequest(Request request) {
        if (request.getRequestType().equals(RequestType.TORTUNE_PRISONER)) {
            printHandling(request);
            request.setHandled(true);
        } else {
            super.handleRequest(request);
        }
    }

    @Override
    public String toString() {
        return "Officer";
    }
}
