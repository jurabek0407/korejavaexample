package Pattern.ChainOfResponsibility.bunkerForExam;

public abstract class RequestHandler {
    private RequestHandler next;

    public RequestHandler(RequestHandler next) {
        this.next = next;
    }

    public void handleRequest(Request request) {
        if (next != null) {
            next.handleRequest(request);
        }
    }

    protected void printRequestType(Request request) {
        System.out.printf("\n%10s | Variant number : %s | Solved by %s ",request.getSubjectTypes() , request.getVariantNumber() , this);
    }

    @Override
    public abstract String toString();
}
