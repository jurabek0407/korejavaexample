package Pattern.ChainOfResponsibility.bunkerForExam;

public class Run {
    public static void main(String[] args) {
        BunkerKing bunkerKing = new BunkerKing();
        bunkerKing.makeSubject(new Request(SubjectTypes.FIZIKA,"3456756"));
        bunkerKing.makeSubject(new Request(SubjectTypes.ONA_TILI,"3456859"));
        bunkerKing.makeSubject(new Request(SubjectTypes.KIMYO,"3458924"));
        bunkerKing.makeSubject(new Request(SubjectTypes.MATEMATIKA,"9876800"));
        bunkerKing.makeSubject(new Request(SubjectTypes.FIZIKA,"9877800"));
        bunkerKing.makeSubject(new Request(SubjectTypes.KIMYO, "8760009"));
    }
}
