package Pattern.ChainOfResponsibility.bunkerForExam;

public class MathTeacher extends RequestHandler{

    public MathTeacher(RequestHandler next) {
        super(next);
    }

    @Override
    public void handleRequest(Request request) {
        if (request.getSubjectTypes().equals(SubjectTypes.MATEMATIKA)){
            printRequestType(request);
            request.setHandled(true);
        }else{
            super.handleRequest(request);
        }
    }

    @Override
    public String toString() {
        return "Math teacher";
    }
}
