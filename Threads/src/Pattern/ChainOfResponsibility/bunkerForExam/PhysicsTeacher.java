package Pattern.ChainOfResponsibility.bunkerForExam;

public class PhysicsTeacher extends RequestHandler {

    public PhysicsTeacher(RequestHandler next) {
        super(next);
    }

    @Override
    public void handleRequest(Request request) {
        if (request.getSubjectTypes().equals(SubjectTypes.FIZIKA)) {
            printRequestType(request);
            request.setHandled(true);
        } else {
            super.handleRequest(request);
        }
    }

    @Override
    public String toString() {
        return "Physics Teacher";
    }
}
