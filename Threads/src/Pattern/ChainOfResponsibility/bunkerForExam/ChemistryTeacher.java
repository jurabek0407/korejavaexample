package Pattern.ChainOfResponsibility.bunkerForExam;

public class ChemistryTeacher extends RequestHandler {
    public ChemistryTeacher(RequestHandler next) {
        super(next);
    }

    @Override
    public void handleRequest(Request request) {
        if (request.getSubjectTypes().equals(SubjectTypes.KIMYO)) {
            printRequestType(request);
            request.setHandled(true);
        } else {
            super.handleRequest(request);
        }
    }

    @Override
    public String toString() {
        return "Chemistry Teacher";
    }
}
