package Pattern.ChainOfResponsibility.bunkerForExam;

public enum SubjectTypes {
    MATEMATIKA,
    FIZIKA,
    KIMYO,
    INGLIZ_TILI,
    ONA_TILI
}
