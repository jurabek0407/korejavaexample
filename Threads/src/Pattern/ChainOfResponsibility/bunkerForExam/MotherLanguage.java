package Pattern.ChainOfResponsibility.bunkerForExam;

public class MotherLanguage extends RequestHandler {
    public MotherLanguage(RequestHandler next) {
        super(next);
    }

    @Override
    public void handleRequest(Request request) {
        if (request.getSubjectTypes().equals(SubjectTypes.ONA_TILI)) {
            printRequestType(request);
            request.setHandled(true);
        } else {
            super.handleRequest(request);
        }
    }

    @Override
    public String toString() {
        return "Mother Language Teacher ";
    }
}
