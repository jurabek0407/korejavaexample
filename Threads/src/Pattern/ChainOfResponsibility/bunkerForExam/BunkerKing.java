package Pattern.ChainOfResponsibility.bunkerForExam;

public class BunkerKing {
    RequestHandler chain;

    public BunkerKing() {
        buildChain();
    }

    private void buildChain() {
        chain = new MathTeacher(new PhysicsTeacher(new ChemistryTeacher(new MotherLanguage(new ForeignTeacher(null)))));
    }

    public void makeSubject(Request request) {
        chain.handleRequest(request);
    }
}
