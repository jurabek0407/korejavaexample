package Pattern.ChainOfResponsibility.bunkerForExam;

public class ForeignTeacher extends RequestHandler {
    public ForeignTeacher(RequestHandler next) {
        super(next);
    }

    @Override
    public void handleRequest(Request request) {
        if (request.getSubjectTypes().equals(SubjectTypes.INGLIZ_TILI)) {
            printRequestType(request);
            request.setHandled(true);
        } else {
            super.handleRequest(request);
        }
    }

    @Override
    public String toString() {
        return "Foreign language teacher";
    }
}
