package Pattern.ChainOfResponsibility.bunkerForExam;

public class Request {
    private final SubjectTypes subjectTypes;
    private final String variantNumber;
    private boolean handled;

    public Request(SubjectTypes subjectTypes, String variantNumber) {
        this.subjectTypes = subjectTypes;
        this.variantNumber = variantNumber;
        this.handled = false;
    }

    public SubjectTypes getSubjectTypes() {
        return subjectTypes;
    }

    public String getVariantNumber() {
        return variantNumber;
    }

    public boolean isHandled() {
        return handled;
    }

    public void setHandled(boolean handled) {
        this.handled = handled;
    }

}
