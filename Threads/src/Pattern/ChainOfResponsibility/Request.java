package Pattern.ChainOfResponsibility;

public class Request {

    private final RequestType requestType;

    private final String requestDescription;

    public Request(RequestType requestType, String requestDescription) {
        this.requestType = requestType;
        this.requestDescription = requestDescription;
    }

    private boolean handled;

    public RequestType getRequestType() {
        return requestType;
    }

    public String getRequestDescription() {
        return requestDescription;
    }
    /*
    *   ko'rib chiqilgan yoki yuqligi.
    */
    public void markHandled() {
        this.handled = true;
    }

    public boolean isHandled() {
        return this.handled;
    }

    @Override
    public String toString() {
        return getRequestDescription();
    }

}
