package Pattern.AbstractFactory;

public interface IBollyWoodMovie {
    String MovieName();
}
