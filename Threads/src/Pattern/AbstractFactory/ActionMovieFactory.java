package Pattern.AbstractFactory;

public class ActionMovieFactory implements IMovieFactory {

    @Override
    public IGollyWoodMovie getGollyWoodMovie() {
        return new GollyWoodActionMovie();
    }

    @Override
    public IBollyWoodMovie getBollyWoodMovie() {
        return new BollyWoodActionMovie();
    }
}
