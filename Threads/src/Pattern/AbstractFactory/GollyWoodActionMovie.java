package Pattern.AbstractFactory;

public class GollyWoodActionMovie  implements IGollyWoodMovie{

    @Override
    public String MovieName() {
        return "GollyWood Action Movie";
    }
}
