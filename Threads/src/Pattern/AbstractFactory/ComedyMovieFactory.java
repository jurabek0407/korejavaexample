package Pattern.AbstractFactory;

public class ComedyMovieFactory {
    public IGollyWoodMovie GetTollywoodMovie()
    {
        return new GollyWoodComedyMovie();
    }
    public IBollyWoodMovie GetBollywoodMovie()
    {
        return new BollyWoodComedyMovie();
    }
}
