package Pattern.AbstractFactory;

public interface IMovieFactory {
    IGollyWoodMovie getGollyWoodMovie();
    IBollyWoodMovie getBollyWoodMovie();
}
