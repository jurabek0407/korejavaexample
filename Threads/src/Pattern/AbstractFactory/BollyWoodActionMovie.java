package Pattern.AbstractFactory;

public class BollyWoodActionMovie implements IBollyWoodMovie{
    @Override
    public String MovieName() {
        return "BollyWood Action Movie";
    }
}
