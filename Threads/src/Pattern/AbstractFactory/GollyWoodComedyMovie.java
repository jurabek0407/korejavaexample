package Pattern.AbstractFactory;

public class GollyWoodComedyMovie implements IGollyWoodMovie{
    @Override
    public String MovieName() {
        return "GollyWood Comedy Movie";
    }
}
