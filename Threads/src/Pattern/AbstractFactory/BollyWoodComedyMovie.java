package Pattern.AbstractFactory;

public class BollyWoodComedyMovie implements IBollyWoodMovie{

    @Override
    public String MovieName() {
        return "BollyWood Comedy Action";
    }
}
