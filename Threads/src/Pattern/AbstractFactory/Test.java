package Pattern.AbstractFactory;

public class Test {
    public static void main(String[] args) {
        ActionMovieFactory actionMovieFactory = new ActionMovieFactory();

        IGollyWoodMovie gAction = actionMovieFactory.getGollyWoodMovie();
        IBollyWoodMovie bAction = actionMovieFactory.getBollyWoodMovie();

        System.out.println(gAction.MovieName());
        System.out.println(bAction.MovieName());
    }
}
