package Pattern.proxy;
public class IvoryTower implements WizardTower {

    public void enter(Wizard wizard) {
        System.out.println("{} enters the tower." + wizard);
    }

}
