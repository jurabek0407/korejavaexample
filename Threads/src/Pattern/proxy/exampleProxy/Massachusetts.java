package Pattern.proxy.exampleProxy;

public class Massachusetts implements University {
    @Override
    public void enter(Student student) {
        System.out.println(student.getFullname() + " entered the massachusetts university of technology. URAAA!!! ");
    }
}
