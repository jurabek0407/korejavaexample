package Pattern.proxy.exampleProxy;

public class MassachusettsProxy implements University {

    private static final int AGE = 30;
    private static final double READING = 7;
    private static final double WRITING = 6.5;
    private static final double LISTINING = 7.5;
    private static final double SPEAKING = 7;
    private static final int GPA = 4;
    private static final int MAX_STUDENT_COUNT = 2500;
    private int numberStudents = 0;
    private University university;

    public MassachusettsProxy(University university) {
        this.university = university;
    }

    @Override
    public void enter(Student student) {
        if (numberStudents <= MAX_STUDENT_COUNT &&
                student.getIelts().getListening() >= LISTINING &&
                student.getIelts().getReading() >= READING &&
                student.getIelts().getSpeaking() >= SPEAKING &&
                student.getIelts().getWriting() >= WRITING &&
                student.getGPA() >= GPA
        ) {
            university.enter(student);
            numberStudents++;
        } else {
            System.out.println(student.getFullname() + " is not allowed to enter University.");
        }
    }
}

