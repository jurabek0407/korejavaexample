package Pattern.proxy.exampleProxy;

public class Run {
    public static void main(String[] args) {
        MassachusettsProxy massachusettsProxy = new MassachusettsProxy(new Massachusetts());
        massachusettsProxy.enter(new Student("Rashidov Jurabek",25,4,new IELTS(8.5,7.5,8,7)));
        massachusettsProxy.enter(new Student("Nizomov Sanjar",19,3,new IELTS(6.5,6,7,7)));
        massachusettsProxy.enter(new Student("Kamolov Behruz",21,5,new IELTS(9,8,7.5,8)));
        massachusettsProxy.enter(new Student("Muzaffarov Shoxruz",22,4,new IELTS(7,9,8,7.5)));
    }
}
