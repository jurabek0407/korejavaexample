package Pattern.proxy.exampleProxy;

public class Student {
    private String fullname;
    private int age;
    private int GPA;
    private IELTS ielts;

    public Student(String fullname, int age, int GPA, IELTS ielts) {
        this.fullname = fullname;
        this.age = age;
        this.GPA = GPA;
        this.ielts = ielts;
    }

    public String getFullname() {
        return fullname;
    }

    public int getAge() {
        return age;
    }

    public int getGPA() {
        return GPA;
    }

    public IELTS getIelts() {
        return ielts;
    }
}
