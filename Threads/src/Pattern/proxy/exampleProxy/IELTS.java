package Pattern.proxy.exampleProxy;

public class IELTS {
    private double reading;
    private double writing;
    private double listening;
    private double speaking;

    public IELTS(double reading, double writing, double listening, double speaking) {
        this.reading = reading;
        this.writing = writing;
        this.listening = listening;
        this.speaking = speaking;
    }

    public double getReading() {
        return reading;
    }

    public double getWriting() {
        return writing;
    }

    public double getListening() {
        return listening;
    }

    public double getSpeaking() {
        return speaking;
    }
}
