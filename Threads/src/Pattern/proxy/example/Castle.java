package Pattern.proxy.example;

public class Castle implements Tower {
    @Override
    public void enter(Student student) {
        System.out.println(student.getName());
    }
}
