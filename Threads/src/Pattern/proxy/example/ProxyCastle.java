package Pattern.proxy.example;

public class ProxyCastle implements Tower {

    Tower castle;
    private final int MAX_COUNT = 4;
    private int numStudent;

    public ProxyCastle(Castle castle) {
        this.castle = castle;
    }

    @Override
    public void enter(Student student) {
        numStudent++;
        if (numStudent < MAX_COUNT) {
            castle.enter(student);
        } else {
            System.out.println("Castle closed!!!");
        }
    }
}
