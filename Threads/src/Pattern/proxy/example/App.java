package Pattern.proxy.example;

public class App {
    public static void main(String[] args) {
        ProxyCastle castle = new ProxyCastle(new Castle());
        castle.enter(new Student("Asliddin"));
        castle.enter(new Student("Sevinch"));
        castle.enter(new Student("Shoxruz"));
        castle.enter(new Student("Asila"));
        castle.enter(new Student("Behruz"));
        castle.enter(new Student("Lola"));
        castle.enter(new Student("Sanjar"));
    }
}
