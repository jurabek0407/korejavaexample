package Pattern.proxy.example;

public interface Tower {
    public void enter( Student student);
}
