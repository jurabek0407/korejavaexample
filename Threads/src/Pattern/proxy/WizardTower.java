package Pattern.proxy;

public interface WizardTower {
    void enter(Wizard wizard);
}
