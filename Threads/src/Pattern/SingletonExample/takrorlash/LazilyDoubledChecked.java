package Pattern.SingletonExample.takrorlash;

public class LazilyDoubledChecked {
    private volatile static LazilyDoubledChecked instance;

    private LazilyDoubledChecked() {
    }

    public static LazilyDoubledChecked getInstance() {
        if (instance == null) {
            synchronized (LazilyDoubledChecked.class) {
                if (instance == null) {
                    instance = new LazilyDoubledChecked();
                }
            }
        }
        return instance;
    }
}
