package Pattern.SingletonExample.takrorlash;

public class Eagerly {
    private Eagerly() {
    }

    private String privateWord = "Biz RSA algoritmidan foydalanyapmiz.";
    private static final Eagerly instance = new Eagerly();

    public static Eagerly getInstance() {
        return instance;
    }

    public String getPrivateWord() {
        return privateWord;
    }
}
