package Pattern.SingletonExample.takrorlash;

public class Lazily {
    public Lazily() {
    }

    private static Lazily instance;

    public static Lazily getInstance() {
        if (instance == null) {
            return new Lazily();
        }
        return instance;
    }
    public String getMessage(){
        return "Message";
    }
}
