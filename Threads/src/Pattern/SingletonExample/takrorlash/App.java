package Pattern.SingletonExample.takrorlash;

public class App {
    public static void main(String[] args) {
        Lazily lazily = Lazily.getInstance();
        System.out.println(lazily.getMessage());
        Lazily lazily1 = Lazily.getInstance();
        System.out.println(lazily1.getMessage());
    }
}
