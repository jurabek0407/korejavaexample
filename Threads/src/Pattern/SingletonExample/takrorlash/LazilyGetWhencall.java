package Pattern.SingletonExample.takrorlash;

public class LazilyGetWhencall {
    private LazilyGetWhencall() {
    }

    private static class SingletonHelper {
        private static final LazilyGetWhencall instance = new LazilyGetWhencall();
    }

    public static LazilyGetWhencall getInstance() {
        return SingletonHelper.instance;
    }

}
