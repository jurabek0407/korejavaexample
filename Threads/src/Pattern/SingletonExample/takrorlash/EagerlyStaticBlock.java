package Pattern.SingletonExample.takrorlash;

public class EagerlyStaticBlock {
    private EagerlyStaticBlock() {
    }

    private static EagerlyStaticBlock instance;

    static {
        try {
            instance = new EagerlyStaticBlock();
        } catch (Exception ex) {
            throw ex;
        }
    }
}
