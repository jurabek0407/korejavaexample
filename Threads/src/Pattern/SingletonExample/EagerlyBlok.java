package Pattern.SingletonExample;

public class EagerlyBlok {
    private EagerlyBlok() {
    }

    private static EagerlyBlok instance;

    /*
    *   static blok bir matra ishlaydi.
    */

    static {
        try {
            instance = new EagerlyBlok();
        } catch (Exception ex) {
            throw ex;
        }
    }

    public static EagerlyBlok getInstance() {
        return instance;
    }
    public String getMessage(){
        return "Blok";
    }
}
