package Pattern.SingletonExample;

public class EagerlySingleton {

    private EagerlySingleton() {
    }

    private static final EagerlySingleton instance = new EagerlySingleton();

    public static EagerlySingleton getInstance() {
        return instance;
    }

}
