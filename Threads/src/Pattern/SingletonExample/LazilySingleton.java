package Pattern.SingletonExample;

public class LazilySingleton {
    private LazilySingleton() {
    }

    private static LazilySingleton instance;

    public static LazilySingleton getInstance() {
        if (instance == null) {
            instance = new LazilySingleton();
        }
        return instance;
    }

}
