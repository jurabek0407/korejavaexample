package Pattern.SingletonExample;

public class MakeACaptain {
    private static MakeACaptain _captain;

    private MakeACaptain() {
    }

    public static MakeACaptain getCaptain() {
        if (_captain == null) {
            _captain = new MakeACaptain();
            System.out.println("New Captain Selected for our team");
        } else {
            System.out.println("You already have a Captain your team");
        }
        return _captain;
    }
}
