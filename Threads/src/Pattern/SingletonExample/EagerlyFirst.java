package Pattern.SingletonExample;

public class EagerlyFirst {
    private EagerlyFirst() {
    }

    private static final EagerlyFirst instance = new EagerlyFirst();

    public static EagerlyFirst getInstance() {
        return instance;
    }
    /*
     *  bunda class load bo'layotganda yaratiladi har safar bu esa vaqtdan yutqazadi.
     */
    public String getMessage(){
        return "First";
    }
}
