package Pattern.SingletonExample;

public class LazilyDoubledChekced {

    private static volatile LazilyDoubledChekced instance;

    private LazilyDoubledChekced() {
    }

    public static LazilyDoubledChekced getInstance() {
        if (instance == null) {
            synchronized (LazilyDoubledChekced.class) {
                if (instance == null) {
                    instance = new LazilyDoubledChekced();
                }
            }
        }
        return instance;
    }
}
