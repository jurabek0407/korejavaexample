package Pattern.SingletonExample;

public class SingletonPatternEx {
    public static void main(String[] args) {

        EagerlyFirst eagerlyFirst1 = EagerlyFirst.getInstance();
        EagerlyFirst eagerlyFirst2 = EagerlyFirst.getInstance();
        EagerlyFirst eagerlyFirst3 = EagerlyFirst.getInstance();
        EagerlyFirst eagerlyFirst4 = EagerlyFirst.getInstance();

        EagerlyBlok eagerlyBlok1 = EagerlyBlok.getInstance();
        EagerlyBlok eagerlyBlok2 = EagerlyBlok.getInstance();
        EagerlyBlok eagerlyBlok3 = EagerlyBlok.getInstance();
        EagerlyBlok eagerlyBlok4 = EagerlyBlok.getInstance();


    }
}
