package Pattern.BridgePattern.Example1;

public class GreenColor implements Color {
    @Override
    public void applyColor() {
        System.out.println("Green");
    }
}
