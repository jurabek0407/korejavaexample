package Pattern.BridgePattern.Example1;

public interface Color {
    public void applyColor();
}
