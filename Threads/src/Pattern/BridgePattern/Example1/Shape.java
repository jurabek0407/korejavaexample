package Pattern.BridgePattern.Example1;

public abstract class Shape {

    protected Color color;

    public Shape(Color c) {
        this.color = c;
    }

    abstract public void applyColor();
}
