package Pattern.BridgePattern.Example1;

public class RedColor implements Color {
    @Override
    public void applyColor() {
        System.out.println("Red");
    }
}
