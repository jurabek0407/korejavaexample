package Pattern.AdapterPattern.Example2;

public class Test {
    public static void main(String[] args) {
        BugattiVeyron bugattiVeyron = new BugattiVeyron();
        System.out.println(bugattiVeyron.getSpeed());

        MovableAdabterImpl movableAdabter = new MovableAdabterImpl(bugattiVeyron);
        System.out.println(movableAdabter.getSpeed());

    }
}
