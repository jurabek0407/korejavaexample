package Pattern.AdapterPattern.Example2;

public interface MovableAdabter {
    double getSpeed();
}
