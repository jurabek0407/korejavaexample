package Pattern.AdapterPattern.Example2;

public interface Movable {
    double getSpeed();
}

