package Pattern.AdapterPattern.Example2;

public class BugattiVeyron implements Movable {
    @Override
    public double getSpeed() {
        return 268;
    }
}
