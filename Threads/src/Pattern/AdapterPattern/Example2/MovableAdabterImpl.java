package Pattern.AdapterPattern.Example2;

public class MovableAdabterImpl implements MovableAdabter {

    private Movable luxuryCars;

    public MovableAdabterImpl(Movable luxuryCars) {
        this.luxuryCars = luxuryCars;
    }

    @Override
    public double getSpeed() {
        return luxuryCars.getSpeed() * 1.609;
    }
}
