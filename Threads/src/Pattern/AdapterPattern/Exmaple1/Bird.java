package Pattern.AdapterPattern.Exmaple1;

public interface Bird {
    public void fly();

    public void makeSound();

}
