package Pattern.strategy;

public interface DragonSlayingStrategy {
    void execute();
}
