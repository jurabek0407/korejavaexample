package Pattern.FactoryPattern;

public class Test {
    public static void main(String[] args) throws Exception {
        IAnimalFactory animalFactory = new ConcreteFactory();

        IAnimal iAnimal = animalFactory.GetAnimalType("Duck");
        iAnimal.speak();

        IAnimal iAnimal1 = animalFactory.GetAnimalType("Tiger");
        iAnimal1.speak();

    }
}
