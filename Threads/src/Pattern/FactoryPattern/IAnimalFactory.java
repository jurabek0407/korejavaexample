package Pattern.FactoryPattern;

abstract class IAnimalFactory {

    public abstract IAnimal

    GetAnimalType(String type) throws Exception;
}