package Pattern.FactoryPattern;

public interface IAnimal {
    void speak();
}
