package Pattern.FactoryPattern.MyExample;

public abstract class Cake {
    String name;
    String type;
    int price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public abstract void recipe();

    public void aboutCake() {
        System.out.printf(" Name:  %s , Type: %s , Price: %d", name, type, price);
    }

}
