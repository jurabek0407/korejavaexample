package Pattern.FactoryPattern.MyExample;

public class CherryCake extends Cake{

    public CherryCake() {
        setName("CherryCake");
        setType("Cherry Cake");
        setPrice(7899);
    }

    @Override
    public void recipe() {
        System.out.println("Cakeni ustiga cherry qo'yamiz");
    }
}
