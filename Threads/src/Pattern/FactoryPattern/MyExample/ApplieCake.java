package Pattern.FactoryPattern.MyExample;

public class ApplieCake extends Cake {

    public ApplieCake() {
        setName("ApplieCake");
        setType("Eggless");
        setPrice(7000);
    }

    @Override
    public void recipe() {
        System.out.println("/*********************************/");
        System.out.println(" Olmani olamiz pishirgan torga qo'yib chiqamiz");
    }

}
