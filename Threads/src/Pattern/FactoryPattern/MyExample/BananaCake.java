package Pattern.FactoryPattern.MyExample;

public class BananaCake extends Cake {

    public BananaCake() {
        setName("BananCake");
        setType("Bananli cake ");
        setPrice(1290);
    }

    @Override
    public void recipe() {
        System.out.println("Cake ni ustiga Banan quyamiz");
    }
}
