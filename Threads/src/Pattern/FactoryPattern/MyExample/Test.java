package Pattern.FactoryPattern.MyExample;

import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Cake cake=null;

        Scanner in = new Scanner(System.in);
        String userOrder = in.nextLine();
        switch (userOrder){
            case "Applie":
                cake = new ApplieCake();
                break;
            case "Cherry":
                cake = new CherryCake();
                break;
            case "Banana":
                cake = new BananaCake();
                break;
            default:
                System.out.println("Bunaqa tort yuq!!!");
        }
        cake.aboutCake();
    }
}
