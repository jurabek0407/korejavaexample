package Pattern.FactoryPattern;

public class Tiger implements IAnimal {
    @Override
    public void speak() {
        System.out.println("Tiger says Hulum Hulum");
    }
}
