package Pattern.FactoryPattern;

public class Duck implements IAnimal {
    @Override
    public void speak() {
        System.out.println("Duck says Puck Puck");
    }
}
