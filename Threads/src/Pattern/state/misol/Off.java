package Pattern.state.misol;

public class Off implements MobileAlertState{
    @Override
    public void alert(AlertStateContext alertStateContext) {
        System.out.println("Turn off");
    }
}
