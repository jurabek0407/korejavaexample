package Pattern.state.misol;

public class AlertStateContext  {
    MobileAlertState obj ;

    public AlertStateContext() {
        this.obj = new Off();
    }
    public void alert(){
        obj.alert(this);
    };
    public void setState(MobileAlertState state){
        obj = state;
    }
}
