package Pattern.state.misol;

public interface MobileAlertState {
    public void alert( AlertStateContext alertStateContext);
}
