package Pattern.state.misol;

public class On implements MobileAlertState {
    @Override
    public void alert(AlertStateContext alertStateContext) {
        System.out.println("Turn on");
    }
}
