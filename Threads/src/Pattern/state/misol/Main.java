package Pattern.state.misol;

public class Main {
    public static void main(String[] args) {
        AlertStateContext alertStateContext = new AlertStateContext();
        alertStateContext.alert();
        alertStateContext.setState(new On());
        alertStateContext.alert();
    }
}
