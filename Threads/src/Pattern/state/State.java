package Pattern.state;
public interface State {

    void onEnterState();

    void observe();

}