package Pattern.facade.exampleFacade;

public class MusicDownloader extends MainDownload{
    @Override
    public String name() {
        return "Music Downloader";
    }
}
