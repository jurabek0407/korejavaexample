package Pattern.facade.exampleFacade;

public class GameDownloader extends MainDownload {
    @Override
    public String name() {
        return "Game downloader";
    }
}
