package Pattern.facade.exampleFacade;

public class Run {
    public static void main(String[] args) {
        Facade facade = new Facade();
        facade.startDownlaod();
        facade.pauseDownlaod();
        facade.startDownlaod();
        facade.cancelDownlaod();
    }
}
