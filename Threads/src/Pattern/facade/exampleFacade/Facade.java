package Pattern.facade.exampleFacade;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Facade {
    private final List<MainDownload> users;
    boolean checkFolder = true;
    public Facade() {
        users = new ArrayList<>();
        users.add(new GameDownloader());
        users.add(new MovieDownloader());
        users.add(new MusicDownloader());
    }

    public void startDownlaod() {
        if (checkFolder){
            createFolderForUser(users);
            checkFolder = false;
        }
        System.out.println();
        makeActions(users, MainDownload.Action.START);
    }

    public void pauseDownlaod() {
        System.out.println();
        makeActions(users, MainDownload.Action.PAUSE);
    }

    public void cancelDownlaod() {
        System.out.println();
        makeActions(users, MainDownload.Action.CANCEL);
    }

    private static void createFolderForUser(Collection<MainDownload> users) {
        for (MainDownload user : users) {
            switch (user.getClass().getSimpleName()) {
                case "GameDownloader":
                    user.createFolderForGame();
                    break;
                case "MovieDownloader":
                    user.createFolderForMovie();
                    break;
                case "MusicDownloader":
                    user.createFolderForMusic();
                    break;
            }
        }
    }

    private static void makeActions(Collection<MainDownload> users, MainDownload.Action  action) {
        for (MainDownload item : users) {
            item.action(action);
        }

    }
}
