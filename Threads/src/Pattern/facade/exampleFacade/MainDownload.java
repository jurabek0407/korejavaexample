package Pattern.facade.exampleFacade;

public abstract class MainDownload {
    public void createFolderForGame() {
        System.out.println("User name : " + name() + "   Created folder for game");
    }

    public void createFolderForMusic() {
        System.out.println("User name : " + name() + "   Created folder for music");
    }

    public void createFolderForMovie() {
        System.out.println("User name : " + name() + "   Created folder for movie");
    }

    public void pause() {
        System.out.println("User name : " + name() + "   Download pause!!!");
    }

    public void start() {
        System.out.println("User name : " + name() + "   Download started");
    }

    public void cancel() {
        System.out.println("User name : " + name() + "   Download cancelled.");
    }

    private void typeDownload(Type type) {
        switch (type) {
            case GAME:
                createFolderForGame();
                break;
            case MUSIC:
                createFolderForMusic();
                break;
            case MOVIE:
                createFolderForMovie();
                break;
        }
    }

    public void action(Action action) {
        switch (action) {
            case START:
                start();
                break;
            case PAUSE:
                pause();
                break;
            case CANCEL:
                cancel();
                break;
        }
    }

    public void action(Action... actions) {
        for (Action action : actions) {
            action(action);
        }
    }

    public void typeDownload(Type... types) {
        for (Type type : types) {
            typeDownload(type);
        }
    }

    public abstract String name();

    enum Action {
        START, PAUSE, CANCEL
    }

    enum Type {
        MUSIC, GAME, MOVIE
    }
}
