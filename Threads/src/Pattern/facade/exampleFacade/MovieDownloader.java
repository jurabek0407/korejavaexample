package Pattern.facade.exampleFacade;

public class MovieDownloader extends MainDownload {
    @Override
    public String name() {
        return "Movie Downloader";
    }
}
