package Pattern.BridgeExample;

public interface Enchantment {
    void onActivate();

    void apply();

    void onDeactivate();
}
