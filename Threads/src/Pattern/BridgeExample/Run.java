package Pattern.BridgeExample;

public class Run {
    public static void main(String[] args) {
        System.out.println("\nThe knight receives an enchanted sword.");
        Sword enchantedSword = new Sword(new SoulEatingEnchantment());
        enchantedSword.wield();
        enchantedSword.swing();
        enchantedSword.unwield();

        System.out.println("\nThe valkyrie receives an enchanted hammer.");
        Hummer hammer = new Hummer(new FlyingEnchantment());
        hammer.wield();
        hammer.swing();
        hammer.unwield();
    }
}
