package Pattern.Decorator.killTank;

public class SimpleL4D2 implements Weapon {
    private String name;
    private double targetDistance;
    private int magazine;
    private double caliber;
    private boolean result;

    public SimpleL4D2(String nameWeapon, double cal, double targetDistance, int magazine) {
        this.name = nameWeapon;
        this.targetDistance = 1000;
        this.magazine = magazine;
        this.caliber = cal;
    }

    public double getCaliber() {
        return caliber;
    }

    public void setCaliber(double caliber) {
        this.caliber = caliber;
    }

    @Override
    public void attack(Tank tank) {
        if (tank.getThickness() < this.caliber){
            System.out.println("Shoot : " + this.caliber + " mm caliber for tank " + tank.getName());
            result = true;
        }

        else{
            System.out.println("Shoot caliber "+this.caliber+" not enough");
        }
    }


}
