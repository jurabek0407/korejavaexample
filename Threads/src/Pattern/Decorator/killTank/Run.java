package Pattern.Decorator.killTank;

public class Run {
    public static void main(String[] args) {
        Tank tank = new Tank("T72B3",90,1000,50);
        Weapon weapon = new SimpleL4D2("L4D2",85,3000,1);
        weapon.attack(tank);
        AdvancedL4D2 advancedWeL4D2 = new AdvancedL4D2(weapon);
        advancedWeL4D2.attack(tank);
    }
}
