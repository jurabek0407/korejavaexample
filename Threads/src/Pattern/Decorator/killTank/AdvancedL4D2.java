package Pattern.Decorator.killTank;

public class AdvancedL4D2 implements Weapon {
    private Weapon weapon;

    public AdvancedL4D2(Weapon weapon) {
        this.weapon = weapon;
    }

    @Override
    public double getCaliber() {
        return weapon.getCaliber();
    }

    @Override
    public void setCaliber(double caliber) {
        weapon.setCaliber(caliber);
    }

    @Override
    public void attack(Tank tank) {
        while (true) {
            setCaliber(getCaliber()+1);
            if (tank.getThickness() < weapon.getCaliber()) {
                System.out.println("Shoot : " + weapon.getCaliber() + " mm caliber enough for tank " + tank.getName());
                break;
            } else {
                System.out.println("shoot caliber " + weapon.getCaliber()+ " not enough");
            }
        }
    }

}
