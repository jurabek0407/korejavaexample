package Pattern.Decorator.killTank;

public class Tank {
    private String name;
    private double thickness;
    private double targetDistance;
    private double speed;

    public Tank(String name, double thickness, double targetDistance, double speed) {
        this.name = name;
        this.thickness = thickness;
        this.targetDistance = targetDistance;
        this.speed = speed;
    }

    public String getName() {
        return name;
    }

    public double getThickness() {
        return thickness;
    }

    public double getTargetDistance() {
        return targetDistance;
    }

    public double getSpeed() {
        return speed;
    }
}
