package Pattern.Decorator.killTank;

public interface Weapon {
    double getCaliber();
    void setCaliber(double caliber);
    void attack(Tank tank);
}
