package Pattern.Decorator;

public class Main {
    public static void main(String[] args) {
        System.out.println("A simple looking troll approaches.");
        Troll troll = new SimpleTroll();
        troll.attack();
        troll.fleeBattle();
        troll.getAttackPower();
        System.out.println("/-----------------------------------/");

        System.out.println("A troll with huge club surprises you.");
        Troll clubbedTroll = new ClubbedTroll(troll);
        clubbedTroll.attack();
        clubbedTroll.fleeBattle();
        System.out.println("Clubbed troll power {}.\n"+ clubbedTroll.getAttackPower());
    }
}
