package Pattern.Decorator;

public class ClubbedTroll implements Troll {

    private Troll decorator;

    public ClubbedTroll(Troll decorator) {
        this.decorator = decorator;
    }

    @Override
    public void attack() {
        decorator.attack();
    }

    @Override
    public int getAttackPower() {
        return decorator.getAttackPower() + 10;
    }

    @Override
    public void fleeBattle() {
        decorator.fleeBattle();
    }
}
