package Pattern.Decorator;

public class SimpleTroll implements Troll {
    @Override
    public void attack() {
        System.out.println("Troll try tries tograb you!!!");
    }

    @Override
    public int getAttackPower() {
        return 10;
    }

    @Override
    public void fleeBattle() {
        System.out.println("The troll shrieks in horror and runs away!");
    }
}
