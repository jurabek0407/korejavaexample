package Pattern.Decorator;

public interface Troll {
    void attack();

    int getAttackPower();

    void fleeBattle();
}
