package Pattern.iterator;

public interface Iterator {
    public boolean hasNext();

    public Object next();
}
