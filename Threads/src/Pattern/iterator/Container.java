package Pattern.iterator;

public interface Container {
    public Iterator getIterator();
}
