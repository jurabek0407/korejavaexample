package Pattern.iterator.example1;

public interface Container {
    Iterator getIterator();
}
