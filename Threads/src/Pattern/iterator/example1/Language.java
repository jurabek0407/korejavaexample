package Pattern.iterator.example1;

public class Language implements Container {
    String[] language = {"Java", "C++", "C", "C#", "PHP"};

    @Override
    public Iterator getIterator() {
        return new LanguageIterator();
    }

    private class LanguageIterator implements Iterator {
        int index;

        @Override
        public boolean hasNext() {
            if (index < language.length) {
                return true;
            }
            return false;
        }

        @Override
        public Object next() {
            if (this.hasNext()) {
                return language[index++];
            } else
                return null;

        }

        @Override
        public String toString() {
            return "LanguageIterator{" +
                    "index=" + index +
                    '}';
        }
    }
}
