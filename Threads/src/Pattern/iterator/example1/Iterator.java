package Pattern.iterator.example1;

public interface Iterator {
    boolean hasNext();

    Object next();
}
