package Pattern.iterator.example1;

public class Run {
    public static void main(String[] args) {
        Language language = new Language();
        Iterator iterator = language.getIterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next().toString());
        }
    }
}
