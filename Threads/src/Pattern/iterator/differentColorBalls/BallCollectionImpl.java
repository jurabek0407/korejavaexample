package Pattern.iterator.differentColorBalls;

import java.util.ArrayList;
import java.util.List;

public class BallCollectionImpl implements BallCollection{
    private List<Ball> ballList;

    public BallCollectionImpl() {
        this.ballList = new ArrayList<>();
    }

    @Override
    public void addBall(Ball ball) {
        ballList.add(ball);
    }

    @Override
    public void removeBall(Ball ball) {
        ballList.remove(ball);
    }

    @Override
    public Iterator iterator(Color color) {
        return new BallIterator(color,ballList);
    }
    private class BallIterator implements Iterator{
        private Color color;
        private List<Ball> list;
        private int position;

        public BallIterator(Color color, List<Ball> list) {
            this.color = color;
            this.list = list;
        }

        @Override
        public boolean hasNext() {
            while (position < list.size()){
                Ball item = list.get(position);
                if (item.getColor().equals(color) || color.equals(Color.ALL)){
                    return true;
                }else{
                    position ++;
                }
            }
            return false;
        }

        @Override
        public Ball next() {
            Ball ball = list.get(position);
            position++;
            return ball;
        }
    }
}
