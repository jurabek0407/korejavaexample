package Pattern.iterator.differentColorBalls;

public class Ball {
    private String numer;
    private Color color;

    public Ball(Color color,String numer) {
        this.color = color;
        this.numer = numer;
    }

    public Color getColor() {
        return color;
    }

    public String getNumer() {
        return numer;
    }
}
