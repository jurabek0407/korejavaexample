package Pattern.iterator.differentColorBalls;

public interface Iterator {
    public boolean hasNext();
    public Ball next();
}
