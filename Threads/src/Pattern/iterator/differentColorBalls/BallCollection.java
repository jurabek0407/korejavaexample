package Pattern.iterator.differentColorBalls;

public interface BallCollection {
    public void addBall(Ball ball);

    public void removeBall(Ball ball);

    public Iterator iterator(Color color);
}
