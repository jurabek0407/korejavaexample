package Pattern.iterator.differentColorBalls;

public class Run {
    public static void main(String[] args) {
        BallCollection ballCollection = getFullCollection();
        Iterator iterator = ballCollection.iterator(Color.BLACK);
        System.out.println("      Only Black ball");
        while (iterator.hasNext()) {
            Ball ball = iterator.next();
            System.out.println("Numer : " + ball.getNumer() + ",  Color : " + ball.getColor());
        }

        System.out.println("      All balls");
        iterator = ballCollection.iterator(Color.ALL);
        while (iterator.hasNext()) {
            Ball ball = iterator.next();
            System.out.println("Numer : " + ball.getNumer() + ",  Color : " + ball.getColor());
        }
    }

    private static BallCollection getFullCollection() {
        BallCollection collection = new BallCollectionImpl();
        collection.addBall(new Ball(Color.RED, "Adidas 1"));
        collection.addBall(new Ball(Color.BLACK, "Adidas 2"));
        collection.addBall(new Ball(Color.GREEN, "Adidas 3"));
        collection.addBall(new Ball(Color.YELLOW, "Adidas 4"));
        collection.addBall(new Ball(Color.BLACK, "Adidas 5"));
        collection.addBall(new Ball(Color.GREEN, "Adidas 6"));
        collection.addBall(new Ball(Color.BLACK, "Adidas 7"));
        collection.addBall(new Ball(Color.GREEN, "Adidas 8"));
        collection.addBall(new Ball(Color.BLACK, "Adidas 9"));
        collection.addBall(new Ball(Color.BLACK, "Adidas 10"));
        return collection;
    }
}
