package Pattern.iterator.differentColorBalls;

public enum Color {
    RED,
    BLACK,
    YELLOW,
    GREEN,
    BLUE,
    ALL
}
