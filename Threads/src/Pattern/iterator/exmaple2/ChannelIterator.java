package Pattern.iterator.exmaple2;

public interface ChannelIterator {
    public boolean hasNext();

    public Channel next();
}
