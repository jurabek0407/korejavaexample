package Pattern.iterator.exmaple2;

public class Channel {

    private double frequence;
    private ChannelTypeEnum TYPE;

    public Channel(double frequence, ChannelTypeEnum TYPE) {
        this.frequence = frequence;
        this.TYPE = TYPE;
    }

    public double getFrequence() {
        return frequence;
    }

    public ChannelTypeEnum getTYPE() {
        return TYPE;
    }

    @Override
    public String toString() {
        return "Frequence : " + this.frequence + " Type : " + this.getTYPE();
    }
}
