package Pattern.flyWeight.bookShelf;

public interface Book {
    public void putBook(BookType type);
}
