package Pattern.flyWeight.bookShelf;

import java.util.EnumMap;
import java.util.Map;

public class BookFactory {
    private final Map<BookType, Book> books;

    public BookFactory() {
        this.books = new EnumMap<BookType, Book>(BookType.class);
    }

    Book createBook(BookType type) {
        Book book = books.get(type);
        if (book == null) {
            switch (type) {
                case DIDAKTIK:
                    book = new DidaktikBook();
                    books.put(type, book);
                    break;
                case BADIIY:
                    book = new BadiiyBook();
                    books.put(type, book);
                    break;
                case SHERIY:
                    book = new SheriyBook();
                    books.put(type, book);
                    break;
                case ROMANTIK:
                    book = new RomantikBook();
                    books.put(type, book);
                    break;
                default:
                    break;
            }
        }
        return book;
    }
}
