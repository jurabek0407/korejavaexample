package Pattern.flyWeight.bookShelf;

import java.util.ArrayList;
import java.util.List;

public class Shelf {
    private List<Book> shelp;
    private List<BookType> types;
    public Shelf() {
        this.shelp = new ArrayList<>();
        this.types = new ArrayList<>();
        fillShelf();
    }
    private void fillShelf(){
        BookFactory bookFactory = new BookFactory();
        shelp.add(bookFactory.createBook(BookType.BADIIY));
        types.add(BookType.BADIIY);
        shelp.add(bookFactory.createBook(BookType.DIDAKTIK));
        types.add(BookType.DIDAKTIK);
        shelp.add(bookFactory.createBook(BookType.SHERIY));
        types.add(BookType.SHERIY);
        shelp.add(bookFactory.createBook(BookType.ROMANTIK));
        types.add(BookType.ROMANTIK);
        shelp.add(bookFactory.createBook(BookType.BADIIY));
        types.add(BookType.BADIIY);
        shelp.add(bookFactory.createBook(BookType.ROMANTIK));
        types.add(BookType.ROMANTIK);
        shelp.add(bookFactory.createBook(BookType.DIDAKTIK));
        types.add(BookType.DIDAKTIK);
        shelp.add(bookFactory.createBook(BookType.SHERIY));
        types.add(BookType.SHERIY);
        shelp.add(bookFactory.createBook(BookType.DIDAKTIK));
        types.add(BookType.DIDAKTIK);
        shelp.add(bookFactory.createBook(BookType.ROMANTIK));
        types.add(BookType.ROMANTIK);
        shelp.add(bookFactory.createBook(BookType.SHERIY));
        types.add(BookType.SHERIY);
        shelp.add(bookFactory.createBook(BookType.SHERIY));
        types.add(BookType.SHERIY);
        shelp.add(bookFactory.createBook(BookType.ROMANTIK));
        types.add(BookType.ROMANTIK);
    }
    public void printShelf(){
        int i=0;
        for (Book item :
                shelp) {
            BookType type = types.get(i++);
            item.putBook(type);
        }
    }

}
