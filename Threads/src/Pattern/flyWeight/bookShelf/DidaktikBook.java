package Pattern.flyWeight.bookShelf;

public class DidaktikBook implements Book {
    @Override
    public void putBook(BookType type) {
        System.out.printf("\nBook Type: %10s , Memory Address : %s" ,type, System.identityHashCode(this));
    }
}
