package Pattern.flyWeight;

public interface Shape {
    void draw();
}

