package Pattern.flyWeight.example;

public interface Potion {

    void drink();
}