package Pattern.flyWeight.example;

public enum PotionType {

    HEALING, INVISIBILITY, STRENGTH, HOLY_WATER, POISON
}