package Pattern.ProtoType;

public class Test {
    public static void main(String[] args) throws CloneNotSupportedException {
        BasicCar f1 = new Ford("Ford 1");
        f1.setPrice(9000);

        Nano n1 = new Nano("Nano 1");
        n1.setPrice(8000);

        BasicCar f2 = f1.clone();
        Ford ford = (Ford) n1.clone();
        System.out.println(ford.modelName + "   ");
//        f2.setPrice(9900);
//        System.out.println(f1.getPrice() + "   " + f1.getModelName());
//        System.out.println(f2.getPrice() + "   " + f2.getModelName());

    }
}
