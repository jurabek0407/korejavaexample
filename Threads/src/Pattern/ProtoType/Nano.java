package Pattern.ProtoType;

public class Nano extends BasicCar{
    public String getColor(){
        return "Red";
    }

    public Nano(String name) {
        modelName = name;
    }

    @Override
    protected BasicCar clone() throws CloneNotSupportedException {
        return (Nano)super.clone();
    }

}
