package Pattern.ProtoType.example;

public class ChildSchool extends School{
    private int price;

    public ChildSchool(String name, int number) {
        super(name, number);
    }

    @Override
    protected School clone() throws CloneNotSupportedException {
        return (ChildSchool) super.clone();
    }
}
