package Pattern.ProtoType.example;

public class School implements Cloneable{
    protected String name;
    protected int number;

    public School(String name, int number) {
        this.name = name;
        this.number = number;
    }

    @Override
    protected School clone() throws CloneNotSupportedException {
        return (School) super.clone();
    }
}
