package Pattern.ProtoType.example;

public class Run {
    public static void main(String[] args) throws CloneNotSupportedException {
        School school = new ChildSchool("Temur Malik", 21);
        System.out.println("Name : " + school.name + " Number : " + school.number);
        ChildSchool childSchool = (ChildSchool) school.clone();
        System.out.println("*-----------------------------*");
        System.out.println("Name : "+childSchool.name + " Number : " + childSchool.number);
    }
}
