package Pattern.BuilderPattern.builderPattern;

final public class Car {
    private String name;
    private String model;
    private int price;


    private Car(Builder builder) {
        this.name = builder.name;
        this.model = builder.model;
        this.price = builder.price;
    }

    @Override
    public String toString() {
        return "Car{" +
                "name='" + name + '\'' +
                ", model='" + model + '\'' +
                ", price=" + price +
                '}';
    }

    public static class Builder {
        private String name;
        private String model;
        private int price;

        private Builder() {
        }

        public static Builder getInctance() {
            return new Builder();
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setModel(String model) {
            this.model = model;
            return this;
        }

        public Builder setPrice(int price) {
            this.price = price;
            return this;
        }

        public Car build() {
            return new Car(this);
        }
    }
}
