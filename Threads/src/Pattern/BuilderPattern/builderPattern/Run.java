package Pattern.BuilderPattern.builderPattern;

public class Run {
    public static void main(String[] args) {
        Car car = Car.Builder.getInctance()
                .setModel("AUDI")
                .setName("A6")
                .setPrice(60000)
                .build();

        System.out.println(car);
    }
}
