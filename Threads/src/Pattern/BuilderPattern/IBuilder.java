package Pattern.BuilderPattern;

public interface IBuilder {
    void BuildBody();

    void InsertWheels();

    void AddHeadLights();

    Product GetVehicle();
}
