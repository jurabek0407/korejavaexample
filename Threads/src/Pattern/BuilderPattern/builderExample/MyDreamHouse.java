package Pattern.BuilderPattern.builderExample;

public class MyDreamHouse implements HouseBuilder {
    House myHouse;

    public MyDreamHouse() {
        this.myHouse = new House();
    }

    @Override
    public void buildFoundation() {
        System.out.println("My house foundation OK.");
    }

    @Override
    public void buildStructure() {
        System.out.println("My house structure  OK.");
    }

    @Override
    public void buildRoof() {
        System.out.println("My house Roof OK.");
    }

    @Override
    public void paintHouse() {
        System.out.println("My House painted OK.");
    }

    @Override
    public void furnishHouse() {
        System.out.println("My House Furnished OK.");
    }

    @Override
    public void priceHouse() {
        System.out.println("My house price 100.000 $");
    }

    @Override
    public House getHouse() {
        return myHouse;
    }
}
