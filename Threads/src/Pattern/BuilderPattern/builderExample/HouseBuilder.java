package Pattern.BuilderPattern.builderExample;

public interface HouseBuilder {
    void buildFoundation();

    void buildStructure();

    void buildRoof();

    void paintHouse();

    void furnishHouse();

    void priceHouse();

    House getHouse();
}
