package Pattern.BuilderPattern.builderExample;

public class ConstructorHouseBuilder {
    private HouseBuilder houseBuilder;

    public ConstructorHouseBuilder(HouseBuilder houseBuilder) {
        this.houseBuilder = houseBuilder;
    }

    public House getHouseBuilder() {
        this.houseBuilder.buildFoundation();
        this.houseBuilder.buildStructure();
        this.houseBuilder.buildRoof();
        this.houseBuilder.paintHouse();
        this.houseBuilder.furnishHouse();
        this.houseBuilder.priceHouse();
        return this.houseBuilder.getHouse();
    }
}
