package Pattern.BuilderPattern.builderExample;

public class Run {
    public static void main(String[] args) {
        ConstructorHouseBuilder constructorHouseBuilder = new ConstructorHouseBuilder(new MyDreamHouse());
        constructorHouseBuilder.getHouseBuilder();
    }
}
