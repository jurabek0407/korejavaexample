package Pattern.BuilderPattern.example;

public class Run {
    public static void main(String args[]) {
        StudentReceiver sr = new StudentReceiver();
        System.out.println(sr.getStudent());
    }
}