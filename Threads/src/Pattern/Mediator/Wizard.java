package Pattern.Mediator;

public class Wizard extends PartyMemberBase {

    @Override
    public String toString() {
        return "Wizard";
    }

}