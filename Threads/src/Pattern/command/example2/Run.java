package Pattern.command.example2;

public class Run {
    public static void main(String[] args) {
        Document document = new Document();
        ActionListenerCommand clickOpen = new ActionOpen(document);
        ActionListenerCommand clickSave = new ActionSave(document);
        clickOpen.execute();
        clickSave.execute();
    }
}
