package Pattern.command.example2;

public interface ActionListenerCommand {
    public void execute();
}
