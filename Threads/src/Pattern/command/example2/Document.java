package Pattern.command.example2;

/*
 *   Receiver class
 */
public class Document {
    public void open() {
        System.out.println("Document opened");
    }

    public void save() {
        System.out.println("Document saved");
    }
}
