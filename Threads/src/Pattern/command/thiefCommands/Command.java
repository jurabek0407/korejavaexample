package Pattern.command.thiefCommands;

public abstract class Command {
    public abstract void start(Target target);
    public abstract void yakan();
    public abstract void getBedana();
    public abstract void zamri();
    public abstract void atanmi();
    public abstract void shuxer();
}
