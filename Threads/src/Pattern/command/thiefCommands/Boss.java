package Pattern.command.thiefCommands;

public class Boss {
    private String name;

    public Boss(String name) {
        this.name = name;
    }

    public void castSpell(Command command, Target target){
        command.start(target);
        command.yakan();
        command.getBedana();
        command.zamri();
        command.shuxer();
    }
}
