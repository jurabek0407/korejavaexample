package Pattern.command.thiefCommands;

public class Run {
    public static void main(String[] args) {
        Command command = new ListenCommand();
        Thief thief = new Thief("newThief");
        Boss boss = new Boss("Pakana");
        boss.castSpell(command,thief);
    }
}
