package Pattern.command.thiefCommands;

public abstract class Target {

    public void checkMoney() {
        System.out.println("All of them money");
    }

    public void getWeapon() {
        System.out.println("Get Weapon");
    }

    public void stop() {
        System.out.println("I stopped");
    }

    public void continuing() {
        System.out.println("I am continuing");
    }

    public void goRun() {
        System.out.println("I am running ");
    }

    @Override
    public abstract String toString();
}
