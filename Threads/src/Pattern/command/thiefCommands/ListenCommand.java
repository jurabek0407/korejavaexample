package Pattern.command.thiefCommands;

public class ListenCommand extends Command {
    Target target;

    @Override
    public void start(Target target) {
        this.target = target;
    }

    @Override
    public void yakan() {
        target.checkMoney();
    }

    @Override
    public void getBedana() {
        target.getWeapon();
    }

    @Override
    public void zamri() {
        target.stop();
    }

    @Override
    public void atanmi() {
        target.continuing();
    }

    @Override
    public void shuxer() {
        target.goRun();
    }
}
