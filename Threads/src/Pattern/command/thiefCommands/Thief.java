package Pattern.command.thiefCommands;

public class Thief extends Target{
    private String name;

    public Thief(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
