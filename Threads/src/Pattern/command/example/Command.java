package Pattern.command.example;

public interface Command {
    public void execute();
}
