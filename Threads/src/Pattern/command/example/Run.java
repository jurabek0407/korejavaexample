package Pattern.command.example;

public class Run {
    public static void main(String[] args) {

        Light light = new Light();
        Stereo stereo = new Stereo();

        SimpleRemoteControl remote = new SimpleRemoteControl();

        remote.setSlot(new LightOnCommand(light));
        remote.buttonWasPressed();

        remote.setSlot(new StereoOffCommand(stereo));
        remote.buttonWasPressed();

    }
}
