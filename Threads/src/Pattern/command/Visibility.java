package Pattern.command;

public enum Visibility {
    VISIBLE("visible"), INVISIBLE("invisible");

    private String name;

    Visibility(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Visibility{" +
                "name='" + name + '\'' +
                '}';
    }
}
