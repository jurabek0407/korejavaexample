package Collectors.Players;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TestPlayers {
    public static void main(String[] args) {
        List<Players> list = new ArrayList<>();
        list.add(new Players("Asliddin",18,1));
        list.add(new Players("Kamol",19,3));
        list.add(new Players("Anvar",17,4));
        list.add(new Players("Doston",25,5));
        list.add(new Players("Vohid",20,2));
        list.add(new Players("Jamol",22,6));
        list.add(new Players("Pulat",23,7));
        System.out.println("Average age =  "+list.stream().collect(Collectors.averagingDouble(s->s.age)));
        list.stream().forEach(a-> System.out.println(a));

    }
}
