package Collectors.Players;

public class Players {
    protected String name;
    protected int age;
    protected int rating;

    public Players(String name, int age, int rating) {
        this.name = name;
        this.age = age;
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "Players{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", rating=" + rating +
                '}';
    }
}
