package Collectors.NamePhoneEmail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestNPE {
    public static void main(String[] args) {
        ArrayList<NamePhoneEmail> myList = new ArrayList<>();
        myList.addAll(Arrays.asList(
                new NamePhoneEmail("Sanjar", "+998909399952", "jurabek_rashidov@mail.ru"),
                new NamePhoneEmail("Asliddin", "+998998725407", "asliddin@mail.ru"),
                new NamePhoneEmail("Sanjar", "+998909009090", "behruzshakarov@mail.ru"),
                new NamePhoneEmail("Sanjar", "+998945656656", "nizomovsanjar@mail.ru")
                )
        );
        Stream<NamePhone> namePhoneStream = myList.stream().map(a -> new NamePhone(a.name, a.phoneNum));
        List<NamePhone> namePhonesList = namePhoneStream.collect(Collectors.toList());
        System.out.println("Name and Phone list");

        for (NamePhone namePhone : namePhonesList
        ) {
            System.out.println("Name : " + namePhone.name + "  Phone : " + namePhone.phoneNum);
        }
        System.out.println("Name Phone Set List~");
        namePhoneStream = myList.stream().map(a->new NamePhone(a.name,a.phoneNum));
        Set<NamePhone> namePhoneSet = namePhoneStream.collect(Collectors.toSet());
        for (NamePhone item : namePhoneSet){
            System.out.println("Name : " + item.name + "  Phone: " + item.phoneNum);
        }
    }
}
