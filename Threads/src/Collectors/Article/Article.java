package Collectors.Article;

public class Article {
    private String name;
    private int wordsCount;

    public Article(String name, int wordsCount) {
        this.name = name;
        this.wordsCount = wordsCount;
    }

    public String getName() {
        return name;
    }

    public int getWordsCount() {
        return wordsCount;
    }
}
