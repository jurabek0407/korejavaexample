package Collectors.ExampleCollector;

import java.util.stream.Stream;

public class TestCollector {
    public static void main(String[] args) {
        String s = Stream.of("Mike", "Nicki", "John")
                .parallel()
                .unordered()
                .collect(new MyCollector());
        System.out.println(s);
    }
}
