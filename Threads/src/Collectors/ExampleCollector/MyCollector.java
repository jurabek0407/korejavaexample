package Collectors.ExampleCollector;

import java.util.EnumSet;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

public class MyCollector implements Collector<String, StringBuffer, String> {
    @Override
    public Supplier<StringBuffer> supplier() {
        return () -> {
            System.out.println("Supplier call");
            return new StringBuffer();
        };
    }

    @Override
    public BiConsumer<StringBuffer, String> accumulator() {
        return (sb, s) -> {
            System.out.println("accumulator function call,"
                    + " accumulator container: "
                    + System.identityHashCode(sb)
                    + " thread: "
                    + Thread.currentThread().getName()
                    + ", processing: " + s);
            sb.append(" ").append(s);
        };
    }

    @Override
    public BinaryOperator<StringBuffer> combiner() {
        return (stringBuilder, s) -> {
            System.out.println("combiner function call");
            return stringBuilder.append(s);
        };
    }

    @Override
    public Function<StringBuffer, String> finisher() {
        return stringBuilder -> stringBuilder.toString();
    }

    @Override
    public Set<Characteristics> characteristics() {
        return EnumSet.of(Characteristics.CONCURRENT);
    }
}
