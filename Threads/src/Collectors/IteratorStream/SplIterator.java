package Collectors.IteratorStream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Spliterator;
import java.util.stream.Stream;

public class SplIterator {
    public static void main(String[] args) {
        ArrayList<String> myList = new ArrayList<>();
        myList.addAll(Arrays.asList("Alfa", "Betta", "Gamma", "Amega","Amega1"));
        Stream<String> myStream = myList.stream();

        Spliterator<String> spliterator = myStream.spliterator();
        Spliterator<String> spliterator1 = spliterator.trySplit();

        System.out.println("splIterator object's elements");

        if (spliterator1 != null){
            spliterator1.forEachRemaining(a-> System.out.println(a));
        }
        System.out.println("SplIterator1 object's elements");

        spliterator.forEachRemaining(a-> System.out.println(a));
    }
}
