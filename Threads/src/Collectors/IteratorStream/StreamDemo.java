package Collectors.IteratorStream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.stream.Stream;

public class StreamDemo {
    public static void main(String[] args) {
        ArrayList<String> myList = new ArrayList<>();
        myList.addAll(Arrays.asList("Alfa", "Betta", "Gamma", "Amega"));
        Stream<String> myStream = myList.stream();

        Iterator<String> itr = myStream.iterator();
        while (itr.hasNext()) {
            System.out.println(itr.next());
        }
        System.out.println("Use Spl Iterator");
        Stream<String> myStream2 = myList.stream();
        Spliterator<String> spliterator = myStream2.spliterator();
        while (spliterator.tryAdvance(a -> System.out.println(a))) ;
    }
}
