package Collectors.Books;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestBook {
    public static void main(String[] args) {
        Book book1 = new Book("Qurbonov Sanjar", 1, 20000);
        Book book2 = new Book("Lolayev Asliddin", 2, 18000);
        Book book3 = new Book("Lolayeva Aziza", 5, 20000);
        Book book4 = new Book("Hamrayeva Lobar", 4, 19000);
        Book book5 = new Book("Lolayev Aziz", 8, 20000);
        Book book6 = new Book("Pardayev Baxodir", 9, 19000);
        Book book7 = new Book("Imomov Jamshid", 15, 20000);
        Book book8 = new Book("Qudratov Kamol", 10, 19000);

        List<Book> books = new ArrayList<>();
        books.addAll(Arrays.asList(book1, book2, book3, book4, book5, book6, book7, book8));

        Map<Double, List<Book>> booksByAuthor = books.stream().collect(Collectors.groupingBy(Book::getPrice));
        Set<Double> key = booksByAuthor.keySet();

        Collection<List<Book>> values = booksByAuthor.values();
        key.stream().forEach(a -> System.out.println(a));

        System.out.println("----------------------------------");

        values.stream().forEach(a -> System.out.println(a.toString()));

    }
}
