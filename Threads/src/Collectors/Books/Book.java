package Collectors.Books;

public class Book {
    private String author;
    private int rank;
    private double price;

    public Book(String author, int rank, double price) {
        this.author = author;
        this.rank = rank;
        this.price = price;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Book{" +
                "author='" + author + '\'' +
                ", rank=" + rank +
                ", price=" + price +
                '}';
    }
}
