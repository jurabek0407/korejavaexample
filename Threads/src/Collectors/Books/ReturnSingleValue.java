package Collectors.Books;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReturnSingleValue{
    public static void main(String[] args) {
        List<Integer> array = new ArrayList<>();
        array.add(101);
        array.add(202);
        array.add(303);
        Stream<Integer> streamCounting = array.stream();
        System.out.println("Counting  : " + streamCounting.collect(Collectors.counting()));
        Stream<Integer> streamMax = array.stream();
        System.out.println("Max  : " + streamMax.collect(Collectors.maxBy(Comparator.naturalOrder())).get());
        Stream<Integer> streamMin = array.stream();
        System.out.println("Min : " + streamMin.collect(Collectors.minBy(Comparator.naturalOrder())).get());
    }
}
