package NetworkProgramming;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.channels.Selector;

public class GreetingClient {
    public static void main(String[] args) {
        System.out.println(args.length);
        String serverName = args[0];
        int port = Integer.parseInt(args[1]);
        try {
            System.out.println("Connection to " + serverName + "  on Port " + port);
            Socket client = new Socket(serverName, port);

            System.out.println("Just Connected to " + client.getRemoteSocketAddress());
            OutputStream outputStream = client.getOutputStream();
            DataOutputStream out = new DataOutputStream(outputStream);

            out.writeUTF("Hello from " + client.getLocalSocketAddress());
            InputStream inFromServer = client.getInputStream();
            DataInputStream in = new DataInputStream(inFromServer);

            System.out.println("Server says " + in.readUTF());
            client.close();

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
