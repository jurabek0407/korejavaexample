package Consumer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

public class ExampleConsumer implements Consumer<String> {
    public static void main(String[] args) {
        Consumer<List<ArrayList<String>>> step1 = (list) -> {
            System.out.println("Step 1");
        };

        Consumer<List<ArrayList<String>>> step2 = (array) -> {
            System.out.println("Step 2");
        };

        Consumer<List<ArrayList<String>>> step3 = (arrayLists -> {
            System.out.println("Step 3");
        });

        Consumer<List<ArrayList<String>>> step4 = (arrayLists -> {
            System.out.println("Step 4");
        });

        Consumer<List<ArrayList<String>>> step5 = (arrayLists -> {
            System.out.println("Step 5");
        });

        Consumer<List<ArrayList<String>>> step6 = (arrayLists -> {
            System.out.println("Step 6");
        });
        List<ArrayList<String>> stringList = new ArrayList<>();

        step6.andThen(step5).andThen(step4).andThen(step3).andThen(step2).andThen(step1).accept(stringList);
        Consumer<LinkedList<Integer>> obj = (parametr) -> {
            parametr.stream().forEach(a -> System.out.println(a));
        };
    }

    @Override
    public void accept(String o) {
        System.out.println("Consumer " + o);
    }
}
