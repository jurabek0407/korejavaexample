package Consumer;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

public class BiConsumerAccept {
    public static void main(String[] args) {
        List<Integer> lista = new ArrayList<>();
        lista.add(101);
        lista.add(202);
        lista.add(303);
        lista.add(404);

        List<Integer> listb = new ArrayList<>();
        listb.add(101);
        listb.add(202);
        listb.add(303);
        listb.add(404);

        BiConsumer<List<Integer>,List<Integer>> equals = (list1,list2)->{
            if (list1.size() != list2.size()){
                System.out.println("False");
            }else{
                for (int i=0; i < list1.size(); i++){
                    if (list1.get(i).compareTo(list2.get(i)) !=0 ){
                        System.out.println("false");
                        return;
                    }
                }
                System.out.println("True");
            }
        };
        equals.accept(lista,listb);
    }
}
