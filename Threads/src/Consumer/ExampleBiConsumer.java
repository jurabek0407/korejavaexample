package Consumer;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

public class ExampleBiConsumer {
    public static void main(String[] args) {

        BiConsumer<List<Integer>, List<String>> biConsumer = (list1, list2) -> {
            list1.stream().forEach(a -> System.out.println(a));
            list2.stream().forEach(a -> System.out.println(a));
        };

        List<Integer> integerArrayList = new ArrayList<>();
        integerArrayList.add(101);
        integerArrayList.add(202);
        integerArrayList.add(303);
        integerArrayList.add(404);

        List<String> stringList = new ArrayList<>();
        stringList.add("Consumer - > bitta parametr keladi.");
        stringList.add("BiConsumer - > ikkita parametr keladi.");
        stringList.add("Accept -> parametr keladi return qilmaydi.");
        stringList.add("andThen - > parametr keladi va qiymat qaytaradi.");

        biConsumer.accept(integerArrayList,stringList);
    }
}
