package Consumer;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

public class AccepAndThenAccept {
    public static void main(String[] args) {
        Consumer<List<Integer>> modify = list -> {
            for (int i = 0; i < list.size(); i++) {
                list.set(i, 2 * list.get(i));
            }
        };

        Consumer<List<Integer>> print = list -> {
            for (int i = 0; i < list.size(); i++) {
                System.out.print("\t" + list.get(i));
            }
        };

        List<Integer> list = new LinkedList<>();
        list.add(101);
        list.add(202);
        list.add(303);
        list.add(404);
        list.add(505);
        modify.andThen(print).accept(list);

    }
}
