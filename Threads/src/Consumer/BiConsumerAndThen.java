package Consumer;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

public class BiConsumerAndThen {
    public static void main(String[] args) {
        List<Integer> listFirst = new ArrayList<>();
        listFirst.add(101);
        listFirst.add(202);
        listFirst.add(303);
        listFirst.add(404);

        List<Integer> listSecond = new ArrayList<>();
        listSecond.add(505);
        listSecond.add(606);
        listSecond.add(707);
        listSecond.add(808);

        BiConsumer<List<Integer>, List<Integer>> modify = (list1, list2) -> {
            for (int i = 0; i < list1.size(); i++) {
                list1.set(i, 2 * list1.get(i));
                list2.set(i, 2 * list1.get(i));
            }
        };

        BiConsumer<List<Integer>, List<Integer>> print = (lista,listb)->{
            lista.stream().forEach(a-> System.out.print(a+"\t"));
            System.out.println();
            listb.stream().forEach(a-> System.out.print(a+"\t"));
        };
        modify.andThen(print).accept(listFirst,listSecond);
    }
}

