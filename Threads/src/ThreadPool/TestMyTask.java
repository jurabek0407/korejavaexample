package ThreadPool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TestMyTask {
    public static void main(String[] args) {
            ExecutorService executorService = Executors.newFixedThreadPool(3);
        for (int i = 0; i < 12; i++) {
            MyTask myTask = new MyTask("task"+(i+1));
            executorService.execute(myTask);
        }
        executorService.shutdown();

    }
}
