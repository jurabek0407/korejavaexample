package annotatsiya.standartAnnotatsion.processing;

import annotatsiya.standartAnnotatsion.documented.Version;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Messager;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.Element;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import java.util.Set;

@SupportedAnnotationTypes({"annotatsiya.standartAnnotatsion.documented.Version"})

public class VersionProcessor extends AbstractProcessor {
    public VersionProcessor() {
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        for (TypeElement currentAnnotation : annotations) {
            Name qualifiedName = currentAnnotation.getQualifiedName();
            if (qualifiedName.contentEquals("annotatsiya.standartAnnotatsion.documented.Version")) {
                Set<? extends Element> annotatedElements;
                annotatedElements = roundEnv.getElementsAnnotatedWith(
                        currentAnnotation);
                for (Element element : annotatedElements) {
                    Version v = element.getAnnotation(Version.class);
                    int major = v.major();
                    int minor = v.minor();
                    if (major < 0 || minor < 0) {
                        String errorMsg = "Version cannot" +
                                " be negative." +
                                " major=" + major +
                                " minor=" + minor;
                        Messager messager =
                                this.processingEnv.getMessager();
                        messager.printMessage(Diagnostic.Kind.ERROR,
                                errorMsg, element);
                    }
                }
            }
        }
        return true;
    }


}
