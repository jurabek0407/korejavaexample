package annotatsiya.standartAnnotatsion.override;

public class C extends A {
    /*
    *   Bu xato beradi chunki biz Override annotatsiyadan foydalnyapmiz.
    *   Biz bilamizki Override annotation shu method super classda bor yoki
    *   yuqligini tekshiradi.
    */
//    @Override
//    public void method3() {
//        super.method3();
//    }
}
