package annotatsiya.standartAnnotatsion.functionalInterface;
@FunctionalInterface
public interface Job {
    void run();
//    void alert();
    /*
    * alert ni ochsak xato beradi chunki @FunctionalInterface faqat interfacelar uchun.
    */
}
