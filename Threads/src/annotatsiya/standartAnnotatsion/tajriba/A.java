package annotatsiya.standartAnnotatsion.tajriba;

import java.lang.annotation.Annotation;

public class A implements Version {
    @Override
    public int major() {
        return 202;
    }

    @Override
    public int minor() {
        return 101;
    }

    @Override
    public Class<? extends Annotation> annotationType() {
        return null;
    }
}
