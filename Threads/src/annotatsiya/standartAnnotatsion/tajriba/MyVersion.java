package annotatsiya.standartAnnotatsion.tajriba;

public @interface MyVersion {
    int minor();

    int major() default 101;
}
