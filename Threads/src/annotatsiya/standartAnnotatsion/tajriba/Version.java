package annotatsiya.standartAnnotatsion.tajriba;

public @interface Version {
    int major();

    int minor() default 101;
}

/*
 *   Meros olib bo'lmaydi
 *   Parametr berib bo'lmaydi
 *   Exception berib bo'lmaydi
 *   Annotatsiya typeda method yozib bo'lmaydi.
 */