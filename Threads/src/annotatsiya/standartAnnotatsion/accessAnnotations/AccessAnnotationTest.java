package annotatsiya.standartAnnotatsion.accessAnnotations;

import java.lang.reflect.Executable;
import java.lang.reflect.Method;

public class AccessAnnotationTest {
    public static void main(String[] args) {

        Class<AccessAnnotation> c = AccessAnnotation.class;
        System.out.println("Annotations for class name : "+c.getName());

        Package p = c.getPackage();
        System.out.println("Annotation for package : " + p.getName());

        System.out.println("Class all method: ");
        Method[]methods = c.getMethods();
        for (Method method :
                methods) {
            System.out.print(method.getName()+"\t");
        }
    }
}
