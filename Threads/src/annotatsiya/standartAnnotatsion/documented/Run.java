package annotatsiya.standartAnnotatsion.documented;

@Version(minor = 101, major = 303)
public class Run {
    public static void main(String[] args) {
        Class<Run> c = Run.class;
        // Get the instance of the Version annotation of Test class
        Version v = c.getAnnotation(Version.class);
        if (v == null) {
            System.out.println("Version annotation is not present.");
        } else {
            int major = v.major();
            int minor = v.minor();
            System.out.println("Version: major=" + major + ", minor=" + minor);
        }
    }
}
