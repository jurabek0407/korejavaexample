package annotatsiya.example;

import java.lang.annotation.Annotation;

public @interface Version  {
    int major();
    int minor();
}
