package ForkJoin;

import java.util.concurrent.*;
public class Summa {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        int[] array = new int[100];
        for (int i = 0; i < 100; i++) {
            array[i] = i;
        }
        ForkJoinPool forkJoinPool = new ForkJoinPool();
        Long text = forkJoinPool.invoke(new ItemSum(0,array.length,array));
        System.out.println(text);
    }
}

class ItemSum extends RecursiveTask<Long> {
    int startLenght;
    int endLenght;
    int[] array;

    public ItemSum(int startLenght, int endLenght, int[] array) {
        this.startLenght = startLenght;
        this.endLenght = endLenght;
        this.array = array;
    }

    @Override
    protected Long compute() {
        if (startLenght <= 10) {
            long sum = 0;
            for (int i = startLenght; i < endLenght; i++) {
                sum += array[i];
            }
            return sum;
        } else {
            int midle = (startLenght + endLenght) / 2;
            ItemSum itemSumLeft = new ItemSum(startLenght, midle, array);
            ItemSum itemSumRight = new ItemSum(midle, endLenght, array);
            itemSumLeft.fork();
            itemSumRight.fork();
            return itemSumLeft.join() + itemSumRight.join();
        }
    }
}
