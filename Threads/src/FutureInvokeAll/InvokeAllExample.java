package FutureInvokeAll;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

public class InvokeAllExample {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        Callable<String> task1 = ()->{
          Thread.sleep(2000);
          return "Result thread 1";
        };

        Callable<String> task2 = ()->{
            Thread.sleep(1000);
            return "Result thread 2";
        };

        Callable<String> task3 = ()->{
            Thread.sleep(5000);
            return "Result Thread 3";
        };

        List<Callable<String>> callableList = Arrays.asList(task1,task2,task3);
        List<Future<String>> futureList = executorService.invokeAll(callableList);

        for (Future<String> item: futureList){
            System.out.println(item.get());
        }
        executorService.shutdown();
    }
}
