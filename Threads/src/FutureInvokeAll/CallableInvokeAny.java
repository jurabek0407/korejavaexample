package FutureInvokeAll;

import java.util.Arrays;
import java.util.concurrent.*;

public class CallableInvokeAny {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(5);

        Callable<String> callable = new MyCallable();
        Future<String> future = executorService.submit(callable);
        System.out.println(future.get());
        executorService.shutdown();
//        Callable<String> task1 = () -> {
//            Thread.sleep(1000);
//            return "Hello from Task 1";
//        };
//        Callable<String> task2 = () -> {
//            Thread.sleep(1000);
//            return "Hello from Task 2";
//        };
//
//        Callable<String> task3 = ()->{
//            Thread.sleep(1000);
//            return "Hello from Task 3";
//        };
//        Callable<String> task4 = ()->{
//            Thread.sleep(1000);
//            return "Hello from Task 4";
//        };

//        String result = executorService.invokeAny(Arrays.asList(task1,task2,task3,task4));
//        System.out.println("Result : " + result);
//        executorService.shutdown();
    }
}
